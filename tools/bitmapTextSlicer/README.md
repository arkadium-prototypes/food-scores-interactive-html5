# Bitmap Text Slicer

## Description

This tool takes in a bitmap font atlas and cuts up each individual character into its own image. You can then add the cut-up characters into your art atlas, to reduce draw calls when using bitmap fonts.

## Instructions

1) Export your bitmap fonts using Littera (http://kvazars.com/littera/)
2) Extract your .fnt and .png files into the "source" folder
3) Double-click "font.exe" application to run the conversion tool
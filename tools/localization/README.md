# Localization

## Description

This tool grabs the texts from your Excel file and converts them into individual JSON files. It works hand-and-hand with the Arkadium LocalizationManager library.

## Instructions

1) Open the SourceTextTranslations.xls and edit the texts
	a) Add new rows for every line of text in your game
	b) Add new columns for more languages
	c) Enclose variables in % (e.g. %VARIABLE_NAME%)
2) Close the Excel file
3) Double-click the MakeLocalizedTexts.bat file
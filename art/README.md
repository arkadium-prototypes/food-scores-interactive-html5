# Art Folder

## Description

Place your cut-up art assets in this folder. Use Texture Packer to pack this art into atlases that are exported to src/assets folder.

Do not place source art here since that should live in Art Share!

## Asset Size Recommendations

0.5x = 640x480 (Standard Arkadium Arena size)
1x = 1280x960 (Twice the Arena size, proven to look good on mobile devices)
2x = 2560x1920 (Twice the standard size, use this for HD graphics if needed)
// ========================= Requirements =========================
var EventBusConstants = require("../constants/EventBusConstants");

// ========================= Construction =========================
var ArenaHelper = function (eventBus) {
	"use strict";
	this._eventBus = eventBus;
	this._pauseHandler = this.handleMidrollStart.bind(this);
	this._resumeHandler = this.handleMidrollFinish.bind(this);

	ARK_game_arena_connector.registerAction("pause", this._pauseHandler);
	ARK_game_arena_connector.registerAction("resume", this._resumeHandler);

	this._eventBus.addListener(EventBusConstants.TopicNames.UI, EventBusConstants.EventNames.UI_BUTTON_PRESSED, this.handleButtonPress, this);
	this._eventBus.addListener(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_GAME_STARTED, this.handleGameStart, this);
	this._eventBus.addListener(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_GAME_ENDED, this.handleGameEnd, this);
	this._eventBus.addListener(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_MIDROLL_REQUESTED, this.handleMidrollRequest, this);
};
ArenaHelper.prototype.constructor = ArenaHelper;
module.exports = ArenaHelper;

// ========================= Methods =========================
ArenaHelper.prototype.handleMidrollRequest = function () {
	"use strict";
	console.log("[ArenaHelper] handleMidrollRequest");
	ARK_game_arena_connector.fireEventToArena("pause_ready");
};

ArenaHelper.prototype.handleMidrollStart = function () {
	"use strict";
	console.log("[ArenaHelper] handleMidrollStart");
	this._eventBus.dispatchEvent(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_MIDROLL_STARTED);
};

ArenaHelper.prototype.handleMidrollFinish = function () {
	"use strict";
	console.log("[ArenaHelper] handleMidrollFinish");
	this._eventBus.dispatchEvent(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_MIDROLL_FINISHED);
};

ArenaHelper.prototype.handleButtonPress = function () {
	"use strict";
	ARK_game_arena_connector.fireEventToArena("event_change");
};

ArenaHelper.prototype.handleGameStart = function () {
	"use strict";
	ARK_game_arena_connector.fireEventToArena("game_start");
};

ArenaHelper.prototype.handleGameEnd = function (score) {
	"use strict";
	ARK_game_arena_connector.changeScore(score);
};

// ========================= Destruction =========================
ArenaHelper.prototype.destroy = function () {
	"use strict";
	this._eventBus.removeListener(EventBusConstants.TopicNames.UI, EventBusConstants.EventNames.UI_BUTTON_PRESSED, this.handleButtonPress, this);
	this._eventBus.removeListener(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_GAME_STARTED, this.handleGameStart, this);
	this._eventBus.removeListener(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_GAME_ENDED, this.handleGameEnd, this);
	this._eventBus.removeListener(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_MIDROLL_REQUESTED, this.handleMidrollRequest, this);
};


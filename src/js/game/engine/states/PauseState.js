// ========================= Requirements =========================
var BaseState = require("./BaseState");

// ========================= Construction =========================
var PauseState = function (data) {
	"use strict";
	BaseState.call(this, PauseState.NAME, data);
};

// ========================= Prototype =========================
module.exports = PauseState;
PauseState.prototype = Object.create(BaseState.prototype);
PauseState.prototype.constructor = PauseState;
PauseState.NAME = "gameController.fsm.state.pause";

// ========================= Methods =========================
PauseState.prototype.enter = function () {
	"use strict";
	BaseState.prototype.enter.call(this);
	this._gameModel.pauseGame();
};

PauseState.prototype.exit = function () {
	"use strict";
	BaseState.prototype.exit.call(this);
};

PauseState.prototype.dispose = function () {
	"use strict";
	BaseState.prototype.dispose.call(this);
};
// ========================= Requirements =========================
var FSMState = require("arkadium-fsm").FSMState;

// ========================= Construction =========================
var BaseState = function (name, data) {
	"use strict";
	FSMState.call(this, name, data);
	if (data !== null && data !== undefined) {
		this._game = data.game;
		this._gameModel = data.gameModel;
	}
};

// ========================= Prototype =========================
module.exports = BaseState;
BaseState.prototype = Object.create(FSMState.prototype);
BaseState.prototype.constructor = BaseState;

// ========================= Methods =========================
BaseState.prototype.enter = function () {
	"use strict";
};

BaseState.prototype.exit = function () {
	"use strict";
};

BaseState.prototype.dispose = function () {
	"use strict";
	this._game = null;
};
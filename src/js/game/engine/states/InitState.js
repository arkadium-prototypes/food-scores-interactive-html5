// ========================= Requirements =========================
var BaseState = require("./BaseState");

// ========================= Construction =========================
var InitState = function (data) {
	"use strict";
	BaseState.call(this, InitState.NAME, data);
};

// ========================= Prototype =========================
module.exports = InitState;
InitState.prototype = Object.create(BaseState.prototype);
InitState.prototype.constructor = InitState;
InitState.NAME = "gameController.fsm.state.firstRun";

// ========================= Methods =========================
InitState.prototype.enter = function () {
	"use strict";
	BaseState.prototype.enter.call(this);
};

InitState.prototype.exit = function () {
	"use strict";
	BaseState.prototype.exit.call(this);
};

InitState.prototype.dispose = function () {
	"use strict";
	BaseState.prototype.dispose.call(this);
	this._game = null;
};
// ========================= Requirements =========================
var BaseState = require("./BaseState");

// ========================= Construction =========================
var GameplayState = function(data) {
    "use strict";
    BaseState.call(this, GameplayState.NAME, data);
};

// ========================= Prototype =========================
module.exports = GameplayState;
GameplayState.prototype = Object.create(BaseState.prototype);
GameplayState.prototype.constructor = GameplayState;
GameplayState.NAME = "gameController.fsm.state.gameplay";

// ========================= Methods =========================
GameplayState.prototype.enter = function () {
    "use strict";
    BaseState.prototype.enter.call(this);
    this._gameModel.resumeGame();
};

GameplayState.prototype.exit = function () {
    "use strict";
    BaseState.prototype.exit.call(this);
};

GameplayState.prototype.dispose = function () {
    "use strict";
    BaseState.prototype.dispose.call(this);
};

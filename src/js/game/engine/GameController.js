// ========================= Requirements =========================
var AnalyticsHelper = require("../analytics/AnalyticsHelper");
var ArenaHelper = require("../arena/ArenaHelper");
var GameContextConstants = require("../constants/GameContextConstants");
var GameControllerFSMConfiguration = require("./GameControllerFSMConfiguration");
var GameSettings = require("../settings/GameSettings");
var EventBusConstants = require("../constants/EventBusConstants");
var FSMModule = require("arkadium-fsm");
var GameplayState = require("./states/GameplayState");
var PauseState = require("./states/PauseState");

// ========================= Construction =========================
var GameController = function (game, model, view) {
	"use strict";
	this._game = game;
	this._model = model;
	this._data = model.data;
	this._view = view;
	this._isDestroyed = false;

	// Events
	this._eventBus = this._game.gameContext[GameContextConstants.EVENT_BUS];
	this._eventBus.addListener(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_GAME_PAUSED, this.handleGamePause, this);
	this._eventBus.addListener(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_GAME_RESUMED, this.handleGameResume, this);

	// States
	var fsmConfig = new GameControllerFSMConfiguration(this._game, this._model);
	var fsmFactory = new FSMModule.FSMFactory();
	this._fsm = fsmFactory.createFSM("gameController.fsm", fsmConfig);

	// Helpers
	this._analyticsHelper = new AnalyticsHelper(this._eventBus, GameSettings.getAspectRatio(), GameSettings.screenWidth, GameSettings.screenHeight);
	this._arenaHelper = new ArenaHelper(this._eventBus);
};

// ========================= Prototype =========================
module.exports = GameController;
GameController.prototype = {};
GameController.prototype.constructor = GameController;

// ========================= Initialization =========================
GameController.prototype.initialize = function () {
	"use strict";
};

// ========================= Event Handlers =========================
GameController.prototype.handleGamePause = function () {
	"use strict";
	this.pauseGame();
};
GameController.prototype.handleGameResume = function () {
	"use strict";
	this.resumeGame();
};

// ========================= Game Flow =========================
GameController.prototype.startGame = function () {
	"use strict";
	this._fsm.goToState(GameplayState.NAME);
	this._eventBus.dispatchEvent(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_GAME_STARTED);
	this.processCalories();
	this.processCarbs();
	this.processProteins();
	this.processFats();
	this.processRecommendations();
};
GameController.prototype.pauseGame = function () {
	"use strict";
	this._fsm.goToState(PauseState.NAME);
};
GameController.prototype.resumeGame = function () {
	"use strict";
	this._fsm.goToState(GameplayState.NAME);
};

GameController.prototype.processCalories = function () {
	"use strict";
	var angle = Math.round(this._model.calories.value / 10) - 35;

	this._game.add.tween(this._view._caloriesArrow).to({angle: angle}, 700, Phaser.Easing.Default, true, 200);
	this._view._caloriesMark.setText(this._model.calories.value + " " + this._model.calories.measure);
};

GameController.prototype.processCarbs = function () {
	"use strict";
	var angle = this._view._carbsArrow.angle + this._model.carbs.value + 30;

	this._game.add.tween(this._view._carbsArrow).to({angle: angle}, 700, Phaser.Easing.Default, true, 200);
	this._view._carbsMark.setText(this._model.carbs.value + " " + this._model.carbs.measure);
};

GameController.prototype.processProteins = function () {
	"use strict";
	var angle = this._view._proteinsArrow.angle + this._model.proteins.value + 60;

	this._game.add.tween(this._view._proteinsArrow).to({angle: angle}, 700, Phaser.Easing.Default, true, 200);
	this._view._proteinsMark.setText(this._model.proteins.value + " " + this._model.proteins.measure);
};

GameController.prototype.processFats = function () {
	"use strict";
	var angle = this._view._fatsArrow.angle + this._model.fats.value + 90;

	this._game.add.tween(this._view._fatsArrow).to({angle: angle}, 700, Phaser.Easing.Default, true, 200);
	this._view._fatsMark.setText(this._model.fats.value + " " + this._model.fats.measure);
};

GameController.prototype.processRecommendations = function () {
	"use strict";
	this._view._footerMark.setText(this._model.calories.value + " " + this._model.calories.measure + " =");
	this._view._bikeLabel.setText(this._model.bike.value + " " + this._model.bike.measure);
	this._view._runningLabel.setText(this._model.running.value + " " + this._model.running.measure);
	this._view._walkingLabel.setText(this._model.walking.value + " " + this._model.walking.measure);
	this._view._trainingLabel.setText(this._model.training.value + " " + this._model.training.measure);
};

// ========================= Destruction =========================
GameController.prototype.destroy = function () {
	"use strict";
	if (this._isDestroyed) {
		return;
	}
	this._isDestroyed = true;

	// Remove events
	this._eventBus.removeListener(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_GAME_PAUSED, this.handleGamePause, this);
	this._eventBus.removeListener(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_GAME_RESUMED, this.handleGameResume, this);

	// Destroy helpers
	this._analyticsHelper.destroy();
	this._arenaHelper.destroy();
};

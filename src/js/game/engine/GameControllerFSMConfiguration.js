// ========================= Requirements =========================
var FSMModule = require("arkadium-fsm");
var FSMConfiguration = FSMModule.FSMConfiguration;
var FSMTransition = FSMModule.FSMTransition;
var InitState = require("./states/InitState");
var GameplayState = require("./states/GameplayState");
var PauseState = require("./states/PauseState");

// ========================= Construction =========================
var GameControllerFSMConfiguration = function(game, gameModel) {
    "use strict";
    FSMConfiguration.call(this);
    this._game = game;
    this._gameModel = gameModel;

    var params = {
        game : this._game,
        gameModel : this._gameModel
    };

    // Add states
    var initState = new InitState(params);
    this.addState(initState);
    var gameplayState = new GameplayState(params);
    this.addState(gameplayState);
    var pauseState = new PauseState(params);
    this.addState(pauseState);
    this.setInitialState(initState);

    // Add transitions
    var transition = new FSMTransition(initState, gameplayState);
    this.addTransition(transition);
    transition = new FSMTransition(gameplayState, pauseState);
    this.addTransition(transition);
    transition = new FSMTransition(pauseState, gameplayState);
    this.addTransition(transition);
    transition = new FSMTransition(gameplayState, gameplayState);
    this.addTransition(transition);
};

// ========================= Prototype =========================
module.exports = GameControllerFSMConfiguration;
GameControllerFSMConfiguration.prototype = Object.create(FSMConfiguration.prototype);
GameControllerFSMConfiguration.prototype.constructor = GameControllerFSMConfiguration;
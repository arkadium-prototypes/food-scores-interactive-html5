// ========================= Requirements =========================
var BaseDisplayObjectContainer = require("../gameobjects/BaseDisplayObjectContainer");
var Button = require("../../core/gui/Button");
var EventBusConstants = require("../constants/EventBusConstants");
/*var GameConstants = require("../constants/GameConstants");*/
var GameContextConstants = require("../constants/GameContextConstants");
var GameSettings = require("../settings/GameSettings");
var OptionsPanel = require("../panels/OptionsPanel");

// ========================= Construction =========================
var GameView = function (game, model) {
	"use strict";
	BaseDisplayObjectContainer.call(this, game);
	this._model = model.data;

	// Cache game contexts
	this._eventBus = this._game.gameContext[GameContextConstants.EVENT_BUS];
	this._panelManager = this._game.gameContext[GameContextConstants.PANEL_MANAGER];
	this._textureAtlasHelper = this._game.gameContext[GameContextConstants.TEXTURE_ATLAS_HELPER];

	// Create layers
	this._backgroundLayer = new Phaser.Group(this._game, this);
	this._gameplayLayer = new Phaser.Group(this._game, this);
	this._uiLayer = new Phaser.Group(this._game, this);
	this._panelsLayer = new Phaser.Group(this._game, this);
	this._panelsLayer.pivot.x = 0.5;
	this._panelsLayer.pivot.y = 0.5;
};

// ========================= Prototype =========================
module.exports = GameView;
GameView.prototype = Object.create(BaseDisplayObjectContainer.prototype);
GameView.prototype.constructor = GameView;

// ========================= Intialization =========================
GameView.prototype.initialize = function () {
	"use strict";
	//Add widgets
	this._introWidget = new Phaser.Group(this._game, this);
	this._mainWidget = new Phaser.Group(this._game, this);
	this._footerWidget = new Phaser.Group(this._game, this);
	this._uiLayer.addChild(this._introWidget);
	this._uiLayer.addChild(this._mainWidget);
	this._uiLayer.addChild(this._footerWidget);

	//Intro Widget
	this._introBackground = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor("bg_part1"), "bg_part1");
	this._dishAsset = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor(this._model.id), this._model.id);
	this._rating = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor("rating_normal"), "rating_normal");
	this._replay = new Button(this._game);
	this._replay.addBacking(this._textureAtlasHelper.getAtlasFor("restart_press"), "restart_press", {tint:"0xFFFFFF"}, {}, {tint:"0xCCCCCC"}, {tint:"0xCCCCCC", offsetY:5});
	this._replay.anchor.setTo(1, 0);
	this._introWidget.addChild(this._introBackground);
	this._introWidget.addChild(this._dishAsset);
	this._introWidget.addChild(this._rating);
	this._introWidget.addChild(this._replay);

	//Main Widget
	this._mainBackground = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor("bg_part2"), "bg_part2");

	this._caloriesIndicator = new Phaser.Group(this._game, this);
	this._caloriesScale = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor("dial"), "dial");
	this._caloriesArrow = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor("arrow"), "arrow");
	this._caloriesArrow.anchor.setTo(0.5, 0.9);
	this._caloriesArrow.pivot.setTo(0.5, 0.9);
	this._caloriesArrow.angle = 270;
	this._caloriesLabel = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor("calories"), "calories");
	this._caloriesMark = new Phaser.Text(this._game, 0, 0, "", {'fontSize': '18px'});
	this._caloriesMark.anchor.setTo(0.5, 0);
	this._caloriesIndicator.addChild(this._caloriesScale);
	this._caloriesIndicator.addChild(this._caloriesArrow);
	this._caloriesIndicator.addChild(this._caloriesLabel);
	this._caloriesIndicator.addChild(this._caloriesMark);

	this._carbsIndicator = new Phaser.Group(this._game, this);
	this._carbsScale = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor("dial"), "dial");
	this._carbsArrow = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor("arrow"), "arrow");
	this._carbsArrow.anchor.setTo(0.5, 0.9);
	this._carbsArrow.pivot.setTo(0.5, 0.9);
	this._carbsArrow.angle = 270;
	this._carbsLabel = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor("carbs"), "carbs");
	this._carbsMark = new Phaser.Text(this._game, 0, 0, "", {'fontSize': '18px'});
	this._carbsMark.anchor.setTo(0.5, 0);
	this._carbsIndicator.addChild(this._carbsScale);
	this._carbsIndicator.addChild(this._carbsArrow);
	this._carbsIndicator.addChild(this._carbsLabel);
	this._carbsIndicator.addChild(this._carbsMark);

	this._proteinsIndicator = new Phaser.Group(this._game, this);
	this._proteinsScale = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor("dial"), "dial");
	this._proteinsArrow = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor("arrow"), "arrow");
	this._proteinsArrow.anchor.setTo(0.5, 0.9);
	this._proteinsArrow.pivot.setTo(0.5, 0.9);
	this._proteinsArrow.angle = 270;
	this._proteinsLabel = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor("proteins"), "proteins");
	this._proteinsMark = new Phaser.Text(this._game, 0, 0, "", {'fontSize': '18px'});
	this._proteinsMark.anchor.setTo(0.5, 0);
	this._proteinsIndicator.addChild(this._proteinsScale);
	this._proteinsIndicator.addChild(this._proteinsArrow);
	this._proteinsIndicator.addChild(this._proteinsLabel);
	this._proteinsIndicator.addChild(this._proteinsMark);

	this._fatsIndicator = new Phaser.Group(this._game, this);
	this._fatsScale = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor("dial"), "dial");
	this._fatsArrow = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor("arrow"), "arrow");
	this._fatsArrow.anchor.setTo(0.5, 0.9);
	this._fatsArrow.pivot.setTo(0.5, 0.9);
	this._fatsArrow.angle = 270;
	this._fatsLabel = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor("fats"), "fats");
	this._fatsMark = new Phaser.Text(this._game, 0, 0, "", {'fontSize': '18px'});
	this._fatsMark.anchor.setTo(0.5, 0);
	this._fatsIndicator.addChild(this._fatsScale);
	this._fatsIndicator.addChild(this._fatsArrow);
	this._fatsIndicator.addChild(this._fatsLabel);
	this._fatsIndicator.addChild(this._fatsMark);

	this._mainWidget.addChild(this._mainBackground);
	this._mainWidget.addChild(this._caloriesIndicator);
	this._mainWidget.addChild(this._carbsIndicator);
	this._mainWidget.addChild(this._proteinsIndicator);
	this._mainWidget.addChild(this._fatsIndicator);

	this._footerBackground = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor("bg_part3"), "bg_part3");
	this._footerMark = new Phaser.Text(this._game, 0, 0, "", {'fontSize': '36px'});
	this._footerMark.anchor.setTo(0.5, 0.5);
	this._icons = new Phaser.Group(this._game, this);
	this._bikeIcon = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor("bike_icon"), "bike_icon");
	this._runningIcon = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor("run_icon"), "run_icon");
	this._walkingIcon = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor("walking_icon"), "walking_icon");
	this._trainingIcon = new Phaser.Image(this._game, 0, 0, this._textureAtlasHelper.getAtlasFor("training_icon"), "training_icon");
	this._bikeLabel = new Phaser.Text(this._game, 0, 0, "", {'fontSize': '24px'});
	this._bikeLabel.anchor.setTo(0.5, 0);
	this._runningLabel = new Phaser.Text(this._game, 0, 0, "", {'fontSize': '24px'});
	this._runningLabel.anchor.setTo(0.5, 0);
	this._walkingLabel = new Phaser.Text(this._game, 0, 0, "", {'fontSize': '24px'});
	this._walkingLabel.anchor.setTo(0.5, 0);
	this._trainingLabel = new Phaser.Text(this._game, 0, 0, "", {'fontSize': '24px'});
	this._trainingLabel.anchor.setTo(0.5, 0);
	this._icons.addChild(this._bikeIcon);
	this._icons.addChild(this._runningIcon);
	this._icons.addChild(this._walkingIcon);
	this._icons.addChild(this._trainingIcon);
	this._icons.addChild(this._bikeLabel);
	this._icons.addChild(this._runningLabel);
	this._icons.addChild(this._walkingLabel);
	this._icons.addChild(this._trainingLabel);
	this._footerWidget.addChild(this._footerBackground);
	this._footerWidget.addChild(this._footerMark);
	this._footerWidget.addChild(this._icons);

	// Add event listeners
	this._eventBus.addListener(EventBusConstants.TopicNames.UI, EventBusConstants.EventNames.UI_OPTIONS_PANEL_CLOSE_REQUESTED, this.handleOptionsPanelCloseRequested, this);
	this._eventBus.addListener(EventBusConstants.TopicNames.UI, EventBusConstants.EventNames.UI_OPTIONS_PANEL_CLOSED, this.handleOptionsPanelClosed, this);

	// Resize elements
	this.onResize();
};

// ========================= Resize =========================
GameView.prototype.onResize = function () {
	"use strict";
	this._introBackground.width = GameSettings.screenWidth;
	this._introBackground.height = GameSettings.screenHeight * 0.4;

	this._dishAsset.height = this._introBackground.height;
	this._dishAsset.width = (this._dishAsset.height * 100) / 66.67;

	this._rating.x = this._dishAsset.width;
	this._rating.height = this._introBackground.height;
	this._rating.width = (this._rating.height * 80.3) / 100;

	this._replay.x = GameSettings.screenWidth - 10;
	this._replay.y = 10;

	this._mainWidget.y = this._introWidget.height;

	this._caloriesIndicator.y = 5;

	this._caloriesScale.width = GameSettings.screenWidth / 6;
	this._caloriesScale.height = (this._caloriesScale.width * 50.4) / 100;
	this._caloriesScale.x = Math.abs((this._caloriesIndicator.width - this._caloriesScale.width) * 0.5) - (5 * GameSettings.objectScale);

	this._caloriesLabel.y = this._caloriesScale.height - 2;
	this._caloriesLabel.width = GameSettings.screenWidth / 4;
	this._caloriesLabel.height = (this._caloriesLabel.width * 19.18) / 100;

	this._caloriesMark.x = this._caloriesIndicator.width * 0.5;
	this._caloriesMark.y = this._caloriesLabel.y + this._caloriesLabel.height;

	this._caloriesArrow.x = this._caloriesIndicator.width * 0.5;
	this._caloriesArrow.y = this._caloriesLabel.y + 10;
	this._caloriesArrow.height = this._caloriesScale.height * 0.7;
	this._caloriesArrow.width = (this._caloriesArrow.height * 42.2) / 100;

	this._carbsIndicator.x = this._carbsIndicator.width;
	this._carbsIndicator.y = 5;

	this._carbsScale.width = GameSettings.screenWidth / 6;
	this._carbsScale.height = (this._carbsScale.width * 50.4) / 100;
	this._carbsScale.x = Math.abs((this._carbsIndicator.width - this._carbsScale.width) * 0.5) - (5 * GameSettings.objectScale);

	this._carbsLabel.y = this._carbsScale.height - 2;
	this._carbsLabel.width = GameSettings.screenWidth / 4;
	this._carbsLabel.height = (this._carbsLabel.width * 19.18) / 100;

	this._carbsMark.x = this._carbsIndicator.width * 0.5;
	this._carbsMark.y = this._carbsLabel.y + this._carbsLabel.height;

	this._carbsArrow.x = this._carbsIndicator.width * 0.5;
	this._carbsArrow.y = this._carbsLabel.y + 10;
	this._carbsArrow.height = this._carbsScale.height * 0.7;
	this._carbsArrow.width = (this._carbsArrow.height * 42.2) / 100;

	this._proteinsIndicator.x = this._carbsIndicator.width * 2;
	this._proteinsIndicator.y = 5;

	this._proteinsScale.width = GameSettings.screenWidth / 6;
	this._proteinsScale.height = (this._proteinsScale.width * 50.4) / 100;
	this._proteinsScale.x = Math.abs((this._proteinsIndicator.width - this._proteinsScale.width) * 0.5) - (5 * GameSettings.objectScale);

	this._proteinsLabel.y = this._proteinsScale.height - 2;
	this._proteinsLabel.width = GameSettings.screenWidth / 4;
	this._proteinsLabel.height = (this._proteinsLabel.width * 19.18) / 100;

	this._proteinsMark.x = this._proteinsIndicator.width * 0.5;
	this._proteinsMark.y = this._proteinsLabel.y + this._proteinsLabel.height;

	this._proteinsArrow.x = this._proteinsIndicator.width * 0.5;
	this._proteinsArrow.y = this._proteinsLabel.y + 10;
	this._proteinsArrow.height = this._proteinsScale.height * 0.7;
	this._proteinsArrow.width = (this._proteinsArrow.height * 42.2) / 100;

	this._fatsIndicator.x = this._proteinsIndicator.width * 3;
	this._fatsIndicator.y = 5;

	this._fatsScale.width = GameSettings.screenWidth / 6;
	this._fatsScale.height = (this._fatsScale.width * 50.4) / 100;
	this._fatsScale.x = Math.abs((this._fatsIndicator.width - this._fatsScale.width) * 0.5) - (5 * GameSettings.objectScale);

	this._fatsLabel.y = this._fatsScale.height - 2;
	this._fatsLabel.width = GameSettings.screenWidth / 4;
	this._fatsLabel.height = (this._fatsLabel.width * 19.18) / 100;

	this._fatsMark.x = this._fatsIndicator.width * 0.5;
	this._fatsMark.y = this._fatsLabel.y + this._fatsLabel.height;

	this._fatsArrow.x = this._fatsIndicator.width * 0.5;
	this._fatsArrow.y = this._fatsLabel.y + 10;
	this._fatsArrow.height = this._fatsScale.height * 0.7;
	this._fatsArrow.width = (this._fatsArrow.height * 42.2) / 100;

	this._mainBackground.width = GameSettings.screenWidth;
	this._mainBackground.height = this._mainWidget.height;

	this._footerWidget.y = this._introWidget.height + this._mainWidget.height;
	this._footerBackground.width = GameSettings.screenWidth;
	this._footerBackground.height = GameSettings.screenHeight - (this._introWidget.height + this._mainWidget.height);



	this._bikeIcon.scale.setTo(1.2, 1.2);
	this._runningIcon.scale.setTo(1.2, 1.2);
	this._walkingIcon.scale.setTo(1.2, 1.2);
	this._trainingIcon.scale.setTo(1.2, 1.2);
	this._runningIcon.x = this._bikeIcon.height * 2;
	this._walkingIcon.x = this._runningIcon.x + this._runningIcon.height * 2;
	this._trainingIcon.x = this._walkingIcon.x + this._walkingIcon.height * 2;

	this._bikeLabel.x = this._bikeIcon.width * 0.5;
	this._bikeLabel.y = this._bikeIcon.height;
	this._runningLabel.x = this._runningIcon.x + this._runningIcon.width * 0.5;
	this._runningLabel.y = this._runningIcon.height;
	this._walkingLabel.x = this._walkingIcon.x + this._walkingIcon.width * 0.5;
	this._walkingLabel.y = this._walkingIcon.height;
	this._trainingLabel.x = this._trainingIcon.x + this._trainingIcon.width * 0.5;
	this._trainingLabel.y = this._trainingIcon.height;

	this._footerMark.x = (GameSettings.screenWidth / 3) * 0.5;
	this._footerMark.y = (this._footerBackground.height * 0.5) - (this._footerMark.height * 0.5);
	this._icons.x = GameSettings.screenWidth / 3;
	this._icons.y = (this._footerBackground.height * 0.5) - (this._icons.height * 0.5);

	this._panelsLayer.scale.x = this._panelsLayer.scale.y = GameSettings.objectScale;
	this._panelsLayer.x = GameSettings.screenWidth * 0.5;
	this._panelsLayer.y = GameSettings.screenHeight * 0.5;
};

// ========================= Update =========================
GameView.prototype.onUpdate = function () {
	"use strict";
};
GameView.prototype.setScrimVisible = function (state) {
	"use strict";
	/*this._scrim.visible = state;
	this._scrim.inputEnabled = state;*/
};

// ========================= Event Handlers =========================
GameView.prototype.handleOptionsButtonPressed = function () {
	"use strict";
	this.setScrimVisible(true);
	this._eventBus.dispatchEvent(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_GAME_PAUSED);
	this._panelManager.open(OptionsPanel.name, {isPortraitMode:GameSettings.getIfPortrait(), scaleFactor:GameSettings.objectScale});
};
GameView.prototype.handleOptionsPanelCloseRequested = function () {
	"use strict";
	this._panelManager.close(OptionsPanel.name);
};
GameView.prototype.handleOptionsPanelClosed = function () {
	"use strict";
	this.setScrimVisible(false);
	this._eventBus.dispatchEvent(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_GAME_RESUMED);
};

// ========================= Getters =========================
GameView.prototype.getPanelsLayer = function () {
	"use strict";
	return this._panelsLayer;
};

// ========================= Destruction =========================
GameView.prototype.destroy = function (destroyChildren) {
	// Exit conditions
	if (this._isDestroyed) {
		return;
	}

	// Remove event listeners
	this._eventBus.removeListener(EventBusConstants.TopicNames.UI, EventBusConstants.EventNames.UI_OPTIONS_PANEL_CLOSE_REQUESTED, this.handleOptionsPanelCloseRequested, this);
	this._eventBus.removeListener(EventBusConstants.TopicNames.UI, EventBusConstants.EventNames.UI_OPTIONS_PANEL_CLOSED, this.handleOptionsPanelClosed, this);

	// Remove layers
	this.removeChild(this._backgroundLayer);
	this.removeChild(this._gameplayLayer);
	this.removeChild(this._uiLayer);
	this.removeChild(this._panelsLayer);

	// Destroy
	BaseDisplayObjectContainer.prototype.destroy.call(this, destroyChildren);
};
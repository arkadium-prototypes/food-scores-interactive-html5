// ========================= Requirements =========================

// ========================= Construction =========================
var GameModel = function (game, data) {
	"use strict";
	this._game = game;
	this.data = data;
	this.calories = null;
	this.carbs = null;
	this.proteins = null;
	this.fats = null;
	this.bike = null;
	this.running = null;
	this.walking = null;
	this.training = null;
	this._isDestroyed = false;
};
module.exports = GameModel;
GameModel.prototype.constructor = GameModel;

// ========================= Initialization =========================
GameModel.prototype.initialize = function () {
	"use strict";
	for(var i = 0; i < this.data.substances.length; i++) {
		var item = this.data.substances[i];

		switch(item.id) {
			case "calories": {
				this.calories = item;
			} break;
			case "proteins": {
				this.proteins = item;
			} break;
			case "carbohydrates": {
				this.carbs = item;
			} break;
			case "fats": {
				this.fats = item;
			} break;
		}
	}

	for(i = 0; i < this.data.consumption.length; i++) {
		var item = this.data.consumption[i];

		switch(item.id) {
			case "walking": {
				this.walking = item;
			} break;
			case "run": {
				this.running = item;
			} break;
			case "bike": {
				this.bike = item;
			} break;
			case "dumbbells": {
				this.training = item;
			} break;
		}
	}
};

// ========================= Update =========================
GameModel.prototype.onUpdate = function () {
	"use strict";
};

// ========================= Game Flow =========================
GameModel.prototype.pauseGame = function () {
	"use strict";
};
GameModel.prototype.resumeGame = function () {
	"use strict";
};

// ========================= Destruction =========================
GameModel.prototype.destroy = function () {
	"use strict";
	// Prevent multiple destructions
	if (this._isDestroyed) { return; }
	this._isDestroyed = true;
};
// ========================= Requirements =========================
var GameSettings = require("../settings/GameSettings");

// ========================= Construction =========================
var AssetsHelper = function () {
	"use strict";
};
module.exports = AssetsHelper;

// ========================= Methods =========================
AssetsHelper.getPathToTextureAsset = function (fileName) {
	"use strict";
	return "assets/images/" + GameSettings.assetScaleTexturePrefix + "/" + fileName;
};

AssetsHelper.getPathToSoundAsset = function (fileName) {
	"use strict";
	return "assets/sounds/" + fileName;
};

AssetsHelper.getPathToFontAsset = function (fileName) {
	"use strict";
	return "assets/images/" + GameSettings.assetScale + "x/fonts/" + fileName;
};

AssetsHelper.getPathToData = function (fileName) {
	"use strict";
	return "assets/data/" + fileName;
};
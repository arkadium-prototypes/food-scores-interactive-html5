var GameContextConstants = {
    EVENT_BUS: "EventBus",
    PANEL_MANAGER: "PanelManager",
    SOUND_MANAGER: "SoundManager",
    STORAGE_MANAGER: "StorageManager",
    TEXTURE_ATLAS_HELPER: "TextureAtlasHelper",
    MODEL_DATA: "DataModel"
};
Object.freeze(GameContextConstants);
module.exports = GameContextConstants;
var DomElementConstants = {
	GAME: "templateGame",
	PRELOADER_CONTAINER: "preloaderContainer",
	SWIFFY_CONTAINER: "swiffyContainer"
};
Object.freeze(DomElementConstants);
module.exports = DomElementConstants;
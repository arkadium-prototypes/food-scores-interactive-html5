// ========================= Manually Set Constants =========================
/**
 * Manually set these constants for your game
 */
var GameConstants = {
	// App constants
	GAME_ID: "GO_TEAM_TEMPLATE",
	VERSION: "0.01",
	IS_DEBUG_MODE: false,
	IS_NATIVE_APP: false,

	// Atlases
	GAME_ATLAS: "gameAtlas",
	DISHES_DATA: "dishes",

	EGG_INDEX_ARRAY: 0,
	CHEESEBURGER_INDEX_ARRAY: 1,
	PASTA_INDEX_ARRAY: 2,
	FISH_INDEX_ARRAY: 3,

	// Dimensions
	BASE_WIDTH: 640,
	BASE_HEIGHT: 480
};
module.exports = GameConstants;

// ========================= Application Enums =========================
GameConstants.AssetScales = {
	SMALL: 0.5,
	MEDIUM: 0.75,
	LARGE: 1
};
Object.freeze(GameConstants.AssetScales);

GameConstants.AssetScaleTexturePrefixes = {
	SMALL: "0.5x",
	MEDIUM: "0.75x",
	LARGE: "1x"
};
Object.freeze(GameConstants.AssetScaleTexturePrefixes);

GameConstants.Devices = {
	DESKTOP: 0,
	TABLET: 1,
	PHONE: 2
};
Object.freeze(GameConstants.Devices);

GameConstants.Orientations = {
	LANDSCAPE: "landscape",
	PORTRAIT: "portrait"
};
Object.freeze(GameConstants.Orientations);

GameConstants.Platforms = {
	WEB: 0, // Standalone web build
	ARENA: 1, // Web build working inside an Arena
	INHABIT: 2, // Web build working inside Inhabit
	ANDROID: 3, // Wrapped for native Android
	IOS: 4, // Wrapped for native iOS
	FACEBOOK: 5 // Web build working inside Facebook canvas
};
Object.freeze(GameConstants.Platforms);

// ========================= Game Enums =========================
/**
 * Change these at your heart's content
 * Make them fit your game
 */
GameConstants.InputPriorities = {
	DEFAULT: 0,
	GAMEPLAY: 1,
	UI: 2,
	SCRIM: 3,
	POPUPS: 4
};
Object.freeze(GameConstants.InputPriorities);

GameConstants.PopupStates = {
	HIDDEN: 0, // Popup is not shown
	APPEARING: 1, // Popup is playing a tween-in animation
	VISIBLE: 2, // Popup is shown onscreen
	DISAPPEARING: 3 // Popup is playing a tween-out animation, returns to Hidden state afterwards
};
Object.freeze(GameConstants.PopupStates);
var EventNames = {
    GAMEPLAY_GAME_STARTED: "gp.gameStarted",
    GAMEPLAY_GAME_ENDED: "gp.gameEnded",

    GAMEPLAY_GAME_PAUSED: "gp.gamePaused",
    GAMEPLAY_GAME_RESUMED: "gp.gameResumed",

    GAMEPLAY_MIDROLL_REQUESTED: "gp.midrollRequested",
    GAMEPLAY_MIDROLL_STARTED: "gp.midrollStarted",
    GAMEPLAY_MIDROLL_FINISHED: "gp.midrollFinished",

    UI_OPTIONS_PANEL_OPEN_REQUESTED: "ui.optionsPanelOpenRequested",
    UI_OPTIONS_PANEL_CLOSE_REQUESTED: "ui.optionsPanelCloseRequested",
    UI_OPTIONS_PANEL_CLOSED: "ui.optionsPanelClosed",

    UI_BUTTON_PRESSED: "ui.buttonPressed",
    UI_ORIENTATION_CHANGED: "ui.orientationChanged"
};
Object.freeze(EventNames);

var TopicNames = {
    GAMEPLAY: "eventBus.topicGameplay",
    UI: "eventBus.topicUI"
};
Object.freeze(TopicNames);

module.exports = {EventNames:EventNames, TopicNames:TopicNames};
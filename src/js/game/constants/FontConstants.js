var FontConstants = {
	UI_FONT: "GrilledCheese",
	SMALL_FONT: "GrilledCheese_small"
};
Object.freeze(FontConstants);
module.exports = FontConstants;
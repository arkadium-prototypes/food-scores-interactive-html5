var SoundConstants = {
    BGM_GAME: "bgm_game",
    BGM_GAME_OVER: "bgm_gameOver",
    SFX_BUTTON_CLICK: "sfx_buttonClicked"
};
Object.freeze(SoundConstants);
module.exports = SoundConstants;
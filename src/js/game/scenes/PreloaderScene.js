// ========================= Requirements =========================
var AssetsHelper = require("./../assets/AssetsHelper");
var DomElementConstants = require("../constants/DomElementConstants");
var EventBus = require("arkadium-eventbus").EventBus;
var EventBusConstants = require("../constants/EventBusConstants");
var FontConstants = require("../constants/FontConstants");
var GameConstants = require("../constants/GameConstants");
var GameContextConstants = require("../constants/GameContextConstants");
var GameSettings = require("../settings/GameSettings");
var GUIBuilder = require("arkadium-gui-builder").GUIBuilder;
var PanelManager = require("arkadium-panelmanager").PanelManager;
var PreloadAnimation = require("./../gameobjects/PreloadAnimation");
var SoundConstants = require("../constants/SoundConstants");
var SoundManager = require("arkadium-sound").SoundManager;
var TextureAtlasHelper = require("../../core/assets/TextureAtlasHelper");

// ========================= Construction =========================
var PreloaderScene = function (game) {
	"use strict";
	this._isAssetLoadingDone = false;
	this._isPreloadAnimationDone = false;
};
module.exports = PreloaderScene;

// ========================= Prototype =========================
PreloaderScene.prototype = {
	// ========================= Preloading =========================
	preload:function () {
		"use strict";
		this.setupGameContext();

		// Show the intro animation
		this.game.stage.setBackgroundColor(0xFFFFFF); // Set background color
		this.showIntroAnimation();

		// Start loading assets
		this.load.enableParallel = true;
		this.load.image("logo", AssetsHelper.getPathToTextureAsset("logo.png"));

		// Load atlases
		var atlasImageUrl = AssetsHelper.getPathToTextureAsset(GameConstants.GAME_ATLAS + ".png");
		var atlasJsonUrl = AssetsHelper.getPathToTextureAsset(GameConstants.GAME_ATLAS + ".json");
		this.load.atlas(GameConstants.GAME_ATLAS, atlasImageUrl, atlasJsonUrl);
		this.load.json(GameConstants.GAME_ATLAS, atlasJsonUrl);
		this._textureAtlasHelper.registerAtlas(GameConstants.GAME_ATLAS, atlasImageUrl, atlasJsonUrl);

		// Load bitmap fonts
		if (GameSettings.useBitmapText) {
			this.load.bitmapFont(FontConstants.UI_FONT, AssetsHelper.getPathToFontAsset("GrilledCheese.png"),
				AssetsHelper.getPathToFontAsset("GrilledCheese.fnt"), null, 0, 0);
			this.load.bitmapFont(FontConstants.SMALL_FONT, AssetsHelper.getPathToFontAsset("GrilledCheese_small.png"),
				AssetsHelper.getPathToFontAsset("GrilledCheese_small.fnt"), null, 0, 0);
		}

		// Load audio
		if (GameSettings.allowSounds) {
			this.load.audio(SoundConstants.BGM_GAME, AssetsHelper.getPathToSoundAsset("bgm_game.mp3"), true);
			this.load.audio(SoundConstants.BGM_GAME_OVER, AssetsHelper.getPathToSoundAsset("bgm_gameOver.mp3"), true);
			this.load.audio(SoundConstants.SFX_BUTTON_CLICK, AssetsHelper.getPathToSoundAsset("sfx_buttonClicked.mp3"), true);
		}

		var dataJsonUrl = AssetsHelper.getPathToData(GameConstants.DISHES_DATA + ".json");
		this.load.json(GameConstants.DISHES_DATA, dataJsonUrl);
	},


	// ========================= Intro Animation =========================
	showIntroAnimation:function () {
		// Initialize variables
		this.showDomElement(DomElementConstants.PRELOADER_CONTAINER);

		// Show preload animation
		if (GameSettings.showPreloadAnimation) {
			// Hide other elements for the preloader to show
			this.showDomElement(DomElementConstants.SWIFFY_CONTAINER);
			this.stage = new swiffy.Stage(document.getElementById(DomElementConstants.SWIFFY_CONTAINER), new PreloadAnimation());
			this.stage.start();

			// Set a timeout for the animation
			var currentScene = this;
			setTimeout(function () {
				currentScene.showDomElement("templateGame");
				currentScene._isPreloadAnimationDone = true;
				currentScene.stage.destroy();
				currentScene.checkIfLoadingIsDone();
			}, 5000);
		}
		// Otherwise, don"t show the animated logo
		else {
			this.showDomElement(DomElementConstants.PRELOADER_CONTAINER);
		}
	},

	/**
	 * Shows one of the main DOM elements and hides the other ones
	 * @param elementName
	 */
	showDomElement:function (elementName) {
		"use strict";
		for (var key in DomElementConstants) {
			if (elementName === DomElementConstants[key]) {
				ARK_gameJQ("#" + DomElementConstants[key]).css("visibility", "visible");
			}
			else {
				ARK_gameJQ("#" + DomElementConstants[key]).css("visibility", "hidden");
			}
		}
	},


	// ========================= Creation =========================
	create:function () {
		"use strict";
		this._isAssetLoadingDone = true;
		this.validateLoadedResources();
		this.showDomElement(DomElementConstants.GAME);
		this.checkIfLoadingIsDone();
	},

	validateLoadedResources:function () {
		"use strict";
		this._textureAtlasHelper.parseJson();
		this._textureAtlasHelper.validate();
	},

	checkIfLoadingIsDone:function () {
		"use strict";
		// If both asset loading and preload animation are done, go to the menu
		if (this._isAssetLoadingDone && (!GameSettings.showPreloadAnimation || (GameSettings.showPreloadAnimation && this._isPreloadAnimationDone) )) {
			this.game.state.start("TitleScene");
		}
	},


	// ========================= Game Contexts =========================
	setupGameContext: function() {
		"use strict";
		// Sound Manager
		var soundManager = SoundManager.instance;
		soundManager.init(this.game);
		this.game.gameContext[GameContextConstants.SOUND_MANAGER] = soundManager;
		
		// Texture Atlas Helper
		this._textureAtlasHelper = new TextureAtlasHelper(this.game);
		this.game.gameContext[GameContextConstants.TEXTURE_ATLAS_HELPER] = this._textureAtlasHelper;

		// Panel Manager
		var parser = new GUIBuilder(this.game, this._textureAtlasHelper);
		var panelManager = new PanelManager(this.game, parser, GameSettings.getAspectRatio());
		this.game.gameContext[GameContextConstants.PANEL_MANAGER] = panelManager;

		// Event Bus
		var eventBus = new EventBus();
		this.game.gameContext[GameContextConstants.EVENT_BUS] = eventBus;
		eventBus.registerEvent(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_GAME_STARTED);
		eventBus.registerEvent(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_GAME_ENDED);
		eventBus.registerEvent(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_GAME_PAUSED);
		eventBus.registerEvent(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_GAME_RESUMED);
		eventBus.registerEvent(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_MIDROLL_STARTED);
		eventBus.registerEvent(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_MIDROLL_FINISHED);
		eventBus.registerEvent(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_MIDROLL_REQUESTED);
		eventBus.registerEvent(EventBusConstants.TopicNames.UI, EventBusConstants.EventNames.UI_OPTIONS_PANEL_OPEN_REQUESTED);
		eventBus.registerEvent(EventBusConstants.TopicNames.UI, EventBusConstants.EventNames.UI_OPTIONS_PANEL_CLOSE_REQUESTED);
		eventBus.registerEvent(EventBusConstants.TopicNames.UI, EventBusConstants.EventNames.UI_OPTIONS_PANEL_CLOSED);
		eventBus.registerEvent(EventBusConstants.TopicNames.UI, EventBusConstants.EventNames.UI_ORIENTATION_CHANGED);
		eventBus.registerEvent(EventBusConstants.TopicNames.UI, EventBusConstants.EventNames.UI_BUTTON_PRESSED);
	}
};
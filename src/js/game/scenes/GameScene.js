// ========================= Requirements =========================
var EventBusConstants = require("../constants/EventBusConstants");
var GameConstants = require("../constants/GameConstants");
var GameContextConstants = require("../constants/GameContextConstants");
var GameController = require("../engine/GameController");
var GameModel = require("../engine/GameModel");
var GameView = require("../engine/GameView");
var GameSettings = require("../settings/GameSettings");
var OptionsPanel = require("../panels/OptionsPanel");

// ========================= Construction =========================
var GameScene = function (game) {};
module.exports = GameScene;

// ========================= Prototype =========================
GameScene.prototype = {
	create:function () {
		"use strict";
		// Cache game context
		this._eventBus = this.game.gameContext[GameContextConstants.EVENT_BUS];
		this._panelManager = this.game.gameContext[GameContextConstants.PANEL_MANAGER];

		// Create the game model
		this._gameModel = new GameModel(this.game, this.game.gameContext[GameContextConstants.MODEL_DATA]);
		this._gameModel.initialize();

		// Create the game view
		this._gameView = new GameView(this.game, this._gameModel);
		this._gameView.initialize();
		this._gameView._replay.OnButtonClicked.add(this.replay, this);
		this.add.existing(this._gameView);

		// Create the game controller
		this._gameController = new GameController(this.game, this._gameModel, this._gameView);
		this._gameController.initialize();

		// Register panels
		this._panelManager.setRoot(this._gameView.getPanelsLayer());
		this._panelManager.registerPanel(OptionsPanel.name, OptionsPanel);
		
		// Start game
		this.resize();
		this._gameController.startGame();
	},
	replay: function() {
		"use strict";
		this.game.state.clearCurrentState();
		this.game.state.start("TitleScene");
	},

	// ========================= Resize =========================
	resize:function () {
		"use strict";
		// Cache dimensions once to reference in the rest of the game
		GameSettings.screenWidth = this.game.width;
		GameSettings.screenHeight = this.game.height;
		GameSettings.objectScale = Math.min(GameSettings.screenWidth / GameConstants.BASE_WIDTH,
				GameSettings.screenHeight / GameConstants.BASE_HEIGHT) / GameSettings.assetScale;

		// Resize views
		this._gameView.onResize();
		this._panelManager.resize(GameSettings.getAspectRatio());

		// Dispatch orientation changed event
		this._eventBus.dispatchEvent(EventBusConstants.TopicNames.UI, EventBusConstants.EventNames.UI_ORIENTATION_CHANGED, GameSettings.getIfPortrait());
	},

	// ========================= Update =========================
	update:function () {
		"use strict";
		this._gameModel.onUpdate();
		this._gameView.onUpdate();
	},

	// ========================= Destruction =========================
	shutdown:function () {
		"use strict";
		if (this._gameModel !== null) {
			this.world.remove(this._gameView);
			this._gameModel.destroy();
			this._gameController.destroy();
		}
	}
};
// ========================= Requirements =========================
var Button = require("../../core/gui/Button");
var FontConstants = require("../constants/FontConstants");
var GameConstants = require("../constants/GameConstants");
var GameContextConstants = require("../constants/GameContextConstants");
var GameSettings = require("../settings/GameSettings");
var SoundConstants = require("../constants/SoundConstants");

// ========================= Construction =========================
var TitleScene = function () { };
module.exports = TitleScene;
var _dishIndexArray = 0;

// ========================= Prototype =========================
TitleScene.prototype = {
	preload:function () {
		"use strict";
		// Cache contexts
		this._soundManager = this.game.gameContext[GameContextConstants.SOUND_MANAGER];
		this._textureAtlasHelper = this.game.gameContext[GameContextConstants.TEXTURE_ATLAS_HELPER];
	},

	create:function () {
		"use strict";
		// Add container
		this._container = new Phaser.Group(this.game, null);
		this.add.existing(this._container);

		// Add background
		var imageName = "bg_title";
		this._background = new Phaser.Image(this.game, 0, 0, this._textureAtlasHelper.getAtlasFor(imageName), imageName);
		this._container.addChild(this._background);

		// Add logo
		// this._logo = new Phaser.Image(this.game, 0, 0, "logo");
		// this._container.addChild(this._logo);

		// Add JSON dishes data
		this._data = this.game.cache.getJSON(GameConstants.DISHES_DATA);

		// Add tiles
		imageName = "cheeseburger";
		this._cheeseburger = new Phaser.Image(this.game, 0, 0, this._textureAtlasHelper.getAtlasFor(imageName), imageName);
		this._cheeseburger.inputEnabled = true;
		this._cheeseburger.events.onInputDown.add(this.handleCheeseburgerPressed, this);
		this._container.addChild(this._cheeseburger);

		imageName = "egg";
		this._egg = new Phaser.Image(this.game, 300, 0, this._textureAtlasHelper.getAtlasFor(imageName), imageName);
		this._egg.inputEnabled = true;
		this._egg.events.onInputDown.add(this.handleEggPressed, this);
		this._container.addChild(this._egg);

		imageName = "fish";
		this._fish = new Phaser.Image(this.game, 600, 0, this._textureAtlasHelper.getAtlasFor(imageName), imageName);
		this._fish.inputEnabled = true;
		this._fish.events.onInputDown.add(this.handleFishPressed, this);
		this._container.addChild(this._fish);

		imageName = "pasta";
		this._pasta = new Phaser.Image(this.game, 900, 0, this._textureAtlasHelper.getAtlasFor(imageName), imageName);
		this._pasta.inputEnabled = true;
		this._pasta.events.onInputDown.add(this.handlePastaPressed, this);
		this._container.addChild(this._pasta);

		// Add play button
		// imageName = "btn_standard";
		// this._playButton = new Button(this.game, GameConstants.InputPriorities.UI);
		// this._playButton.addBacking(this._textureAtlasHelper.getAtlasFor(imageName), imageName, {tint:"0xFFFFFF"}, {}, {tint:"0xCCCCCC"}, {tint:"0xCCCCCC", offsetY:5});
		// this._playButton.addLabel(LocalizationManager.getText("button.play"), {
		// 	bitmapText:GameSettings.useBitmapText, font:FontConstants.UI_FONT, fontSize:60, fill:0xFFFFFF, align:"center"
		// });
		// this._playButton.OnButtonClicked.add(this.handlePlayButtonPressed, this);
		// this._container.addChild(this._playButton);

		// Call resize to make sure all elements are positioned correctly
		this.resize();
	},

	// ========================= Resize =========================
	resize:function () {
		"use strict";
		// Cache dimensions once to reference in the rest of the game
		GameSettings.screenWidth = this.game.width;
		GameSettings.screenHeight = this.game.height;
		GameSettings.objectScale = Math.min(GameSettings.screenWidth / GameConstants.BASE_WIDTH,
				GameSettings.screenHeight / GameConstants.BASE_HEIGHT) / GameSettings.assetScale;

		// Resize background
		this._background.width = GameSettings.screenWidth;
		this._background.height = GameSettings.screenHeight;

		// Resize logo
		// this._logo.scale.x = this._logo.scale.y = GameSettings.objectScale;
		// this._logo.x = (GameSettings.screenWidth - this._logo.width) * 0.5;
		// this._logo.y = (GameSettings.screenHeight - this._logo.height) * 0.5 - GameSettings.mri(50);

		// Resize play button
		// this._playButton.onResize(GameSettings.screenWidth * 0.5, GameSettings.screenHeight * 0.15);
		// this._playButton.x = GameSettings.screenWidth * 0.5;
		// this._playButton.y = GameSettings.screenHeight - this._playButton.height;

		// Resize tiles
		var scale = (GameSettings.objectScale * 0.9);
		this._cheeseburger.scale.x = this._cheeseburger.scale.y = scale;
		this._cheeseburger.x = (GameSettings.screenWidth - this._cheeseburger.width) * 0 + GameSettings.mri(25);
		this._cheeseburger.y = (GameSettings.screenHeight - this._cheeseburger.height) * 0.25;

		this._egg.scale.x = this._egg.scale.y = scale;
		this._egg.x = (GameSettings.screenWidth - this._egg.width) * 1 - GameSettings.mri(25);
		this._egg.y = (GameSettings.screenHeight - this._egg.height) * 0.25;

		this._fish.scale.x = this._fish.scale.y = scale;
		this._fish.x = (GameSettings.screenWidth - this._fish.width) * 0 + GameSettings.mri(25);
		this._fish.y = (GameSettings.screenHeight - this._fish.height) * 1 - GameSettings.mri(25);

		this._pasta.scale.x = this._pasta.scale.y = scale;
		this._pasta.x = (GameSettings.screenWidth - this._pasta.width) * 1 - GameSettings.mri(25);
		this._pasta.y = (GameSettings.screenHeight - this._pasta.height) * 1 - GameSettings.mri(25);
	},

	// ========================= Next Scene =========================
	startNextScene:function (dishIndexArray) {
		"use strict";
		// Start music
		if (GameSettings.allowSounds) {
			this._soundManager.playSound(SoundConstants.SFX_BUTTON_CLICK, 1);
			this._soundManager.playSound(SoundConstants.BGM_GAME, 1, true);
		}

		var data = this._data.dishes[dishIndexArray];
		this.game.gameContext[GameContextConstants.MODEL_DATA] = data;

		// Switch scenes
		this.game.state.clearCurrentState();
		this.game.state.start("GameScene");
	},

	// ========================= Event =========================
	handleCheeseburgerPressed:function () {
		"use strict";
		this.startNextScene(GameConstants.CHEESEBURGER_INDEX_ARRAY);
	},

	handleEggPressed:function () {
		"use strict";
		this.startNextScene(GameConstants.EGG_INDEX_ARRAY);
	},

	handleFishPressed:function () {
		"use strict";
		this.startNextScene(GameConstants.FISH_INDEX_ARRAY);
	},

	handlePastaPressed:function () {
		"use strict";
		this.startNextScene(GameConstants.PASTA_INDEX_ARRAY);
	},

	// ========================= Destruction =========================
	shutdown:function() {
		"use strict";
		// this._playButton.OnButtonClicked.remove(this.handlePlayButtonPressed, this);
		// this._playButton.destroy();

		this._cheeseburger.events.onInputDown.remove(this.handleCheeseburgerPressed, this);
		this._egg.events.onInputDown.remove(this.handleCheeseburgerPressed, this);
		this._fish.events.onInputDown.remove(this.handleCheeseburgerPressed, this);
		this._pasta.events.onInputDown.remove(this.handleCheeseburgerPressed, this);
	}
};

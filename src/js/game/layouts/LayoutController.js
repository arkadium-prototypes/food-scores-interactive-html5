// ========================= Requirements =========================
var GameSettings = require("../settings/GameSettings");
var LandscapeLayout = require("./LandscapeLayout");
var PortraitLayout = require("./PortraitLayout");

// ========================= Construction =========================
/**
 * Selects which layout to show (depending on device and orientation)
 * Owns all the panels in the game
 * @param game
 * @constructor
 */
var LayoutController = function(game){
    "use strict";
    this._game = game;
    this._currentLayout = null;
    this._allLayouts = [];

    // Layouts
    this.landscapeLayout = LandscapeLayout;
    this.portraitLayout = PortraitLayout;
};
module.exports = LayoutController;

// ========================= Prototype =========================
LayoutController.prototype = {
    // ========================= Initialization =========================
	initialize:function() {
		"use strict";
    },

    // ========================= Setters =========================
    setLayout: function(layout) {
        "use strict";
        // Prevent switching to the same layout
        if (this._currentLayout === this._allLayouts[layout.Key]) {
            console.log("[LayoutController]", "setLayout()", "Warning: Cannot switch to the same layout", layout.Key);
            return;
        }

        // Create the layout if it doesn't exist
        if (!this._allLayouts[layout.Key]) {
            this._allLayouts[layout.Key] = new layout(this._game, this);
        }

        // Switch to the new layout
        this._currentLayout = this._allLayouts[layout.Key];
    },

    // ========================= Resize =========================
    /**
     * event handler, handles on resize that happens when canvas been resized or screen orientation changed
     */
    onResize: function (width, height) {
        "use strict";
        // Determine layout by orientation
        if (GameSettings.getIfPortrait()) {
            this.setLayout(this.portraitLayout);
        }
		else {
			this.setLayout(this.landscapeLayout);
		}

        // Resize some elements regardless of layouts changing or not
        this._currentLayout.onResize(width, height); // Resize layout

    },

	// ========================= Update =========================
	onUpdate: function() {
		"use strict";
	},

    // ========================= Destruction =========================
    destroy: function () {
        "use strict";
    }
};
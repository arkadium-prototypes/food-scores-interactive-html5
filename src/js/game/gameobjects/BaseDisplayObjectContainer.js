// ========================= Requirements =========================

// ========================= Construction =========================
var BaseDisplayObjectContainer = function (game) {
	"use strict";
	Phaser.Group.call(this, game, null);
	this._game = game;
	this._isDestroyed = false;
};

// ========================= Prototype =========================
module.exports = BaseDisplayObjectContainer;
BaseDisplayObjectContainer.prototype = Object.create(Phaser.Group.prototype);
BaseDisplayObjectContainer.prototype.constructor = BaseDisplayObjectContainer;

// ========================= Methods =========================
BaseDisplayObjectContainer.prototype.initialize = function () {
	"use strict";
};

BaseDisplayObjectContainer.prototype.destroy = function (destroyChildren) {
	"use strict";
	this._isDestroyed = true;
	Phaser.Group.prototype.destroy.call(this, destroyChildren);
};
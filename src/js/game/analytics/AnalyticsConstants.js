var AnalyticsConstants = {
    EVENT_GAMEPLAY_START : "gp.start",
    EVENT_GAMEPLAY_GAME_END : "gp.gameEnd",
    EVENT_UI_SHOW_HELP_PANEL : "ui.showHelpPanel",
    EVENT_UI_ORIENTATION_CHANGED : "ui.orientationChanged"
};
Object.freeze(AnalyticsConstants);
module.exports = AnalyticsConstants;
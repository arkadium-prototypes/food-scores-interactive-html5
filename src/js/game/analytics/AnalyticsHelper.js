// ========================= Requirements =========================
var AnalyticsConstants = require("./AnalyticsConstants");
var AnalyticsManager = require("../../core/analytics/AnalyticsManager");
var EventBusConstants = require("../constants/EventbusConstants");

// ========================= Construction =========================
var AnalyticsHelper = function (eventBus, screenOrientation, screenWidth, screenHeight) {
	"use strict";
	// Initialize variables
	this._gameplayStartTime = null;
	this._screenOrientation = screenOrientation;
	this._screenWidth = screenWidth;
	this._screenHeight = screenHeight;

	// Cache game contexts
	var config = {
		provider:AnalyticsManager.Providers.AMPLITUDE,
		amplitude:{appId:""}
	};
	this._analyticsManager = new AnalyticsManager();
	this._analyticsManager.init(config);

	// Add events
	this._eventBus = eventBus;
	this._eventBus.addListener(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_GAME_STARTED, this.handleGameStarted, this);
	this._eventBus.addListener(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_GAME_ENDED, this.handleGameEnded, this);
	this._eventBus.addListener(EventBusConstants.TopicNames.UI, EventBusConstants.EventNames.UI_ORIENTATION_CHANGED, this.handleOrientationChange, this);
	this._eventBus.addListener(EventBusConstants.TopicNames.UI, EventBusConstants.EventNames.UI_OPTIONS_PANEL_OPEN_REQUESTED, this.handleShowOptionsPanel, this);
};

// ========================= Prototype =========================
module.exports = AnalyticsHelper;
AnalyticsHelper.prototype.constructor = AnalyticsHelper;

// ========================= Event Handlers =========================
AnalyticsHelper.prototype.handleGameStarted = function () {
	"use strict";
	this.sendStartGameEvent();
};
AnalyticsHelper.prototype.sendStartGameEvent = function () {
	"use strict";
	this._gameplayStartTime = new Date().getTime();
	this._analyticsManager.logEvent(
		AnalyticsConstants.EVENT_GAMEPLAY_START,
		{
			screenOrientation:this._screenOrientation,
			screenWidth:this._screenWidth,
			screenHeight:this._screenHeight
		}
	);
};

AnalyticsHelper.prototype.handleGameEnded = function (score) {
	"use strict";
	this.sendGameEndEvent(score);
};
AnalyticsHelper.prototype.sendGameEndEvent = function (score) {
	"use strict";
	var time = Math.round((new Date().getTime() - this._gameplayStartTime) / 1000);
	this._analyticsManager.logEvent(AnalyticsConstants.EVENT_GAMEPLAY_GAME_END, {duration:time, score:score});
};

AnalyticsHelper.prototype.handleOrientationChange = function () {
	"use strict";
	this.sendOrientationChangedEvent();
};
AnalyticsHelper.prototype.sendOrientationChangedEvent = function (isPortrait) {
	"use strict";
	this._analyticsManager.logEvent(AnalyticsConstants.EVENT_UI_ORIENTATION_CHANGED, {orientation:isPortrait ? "portrait" : "landscape"});
};

AnalyticsHelper.prototype.handleShowOptionsPanel = function () {
	"use strict";
	this.sendShowOptionsPanelEvent();
};
AnalyticsHelper.prototype.sendShowOptionsPanelEvent = function () {
	"use strict";
	var time = Math.round((new Date().getTime() - this._gameplayStartTime) / 1000);
	this._analyticsManager.logEvent(AnalyticsConstants.EVENT_UI_SHOW_HELP_PANEL, {timeFromStart:time});
};

// ========================= Destruction =========================
AnalyticsHelper.prototype.destroy = function () {
	"use strict";
	// Remove event listeners
	this._eventBus.removeListener(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_GAME_STARTED, this.handleGameStarted, this);
	this._eventBus.removeListener(EventBusConstants.TopicNames.GAMEPLAY, EventBusConstants.EventNames.GAMEPLAY_GAME_ENDED, this.handleGameEnded, this);
	this._eventBus.removeListener(EventBusConstants.TopicNames.UI, EventBusConstants.EventNames.UI_ORIENTATION_CHANGED, this.handleOrientationChange, this);
	this._eventBus.removeListener(EventBusConstants.TopicNames.UI, EventBusConstants.EventNames.UI_OPTIONS_PANEL_OPEN_REQUESTED, this.handleShowOptionsPanel, this);

	// Nullify references
	this._analyticsManager = null;
	this._eventBus = null;
};
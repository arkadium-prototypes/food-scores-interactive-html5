// ========================= Object =========================
var GameSettings = {
	// Atlases
	assetScale:1,
	assetScaleTexturePrefix:"1",

	// App variables
	currentBrowserName:"",
	currentDevice:0,
	currentPlatform:0,
	objectScale:0,
	screenWidth:0,
	screenHeight:0,

	// Game variables (automatically set)
	allowInterstitialAds:false,
	allowPurchasing:false,
	allowSounds:false,
	hasRetinaDisplay:false,
	isWebGLEnabled:true,
	isWebKitBrowser:false,
	isChromeBrowser:false,
	isLocalStorageEnabled:true,
	showPreloadAnimation:false,
	useBitmapText:true
};
module.exports = GameSettings;

// ========================= Methods =========================
GameSettings.mri = function (value) {
	return value * GameSettings.assetScale;
};
GameSettings.getIfPortrait = function () {
	return GameSettings.screenWidth < GameSettings.screenHeight;
};
GameSettings.getAspectRatio = function () {
	return (GameSettings.getIfPortrait) ? ("portrait") : ("landscape");
};
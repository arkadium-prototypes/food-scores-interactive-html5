// ========================= Construction =========================
var UserSettings = {
	wereOptionsChanged: false
};
module.exports = UserSettings;

// ========================= Options =========================
UserSettings.Options = {
	isSoundsEnabled: true,
	showHelpAtStart: true
};
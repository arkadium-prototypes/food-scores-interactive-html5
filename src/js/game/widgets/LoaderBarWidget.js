// ========================= Requirements =========================
var BaseDisplayObjectContainer = require("../gameobjects/BaseDisplayObjectContainer");
var GameSettings = require("../settings/GameSettings");

// ========================= Construction =========================
var LoaderBarWidget = function (game) {
	"use strict";
	BaseDisplayObjectContainer.call(this, game);
};

// ========================= Prototype =========================
module.exports = LoaderBarWidget;
LoaderBarWidget.prototype = Object.create(BaseDisplayObjectContainer.prototype);
LoaderBarWidget.prototype.constructor = LoaderBarWidget;

// ========================= Initialization =========================
LoaderBarWidget.prototype.initialize = function () {
	"use strict";
	this._progressBarFrame = new Phaser.Image(this._game, 0, 0, "progressBarFrame");
	this.addChild(this._progressBarFrame);
	this._progressBar = new Phaser.Image(this._game, GameSettings.mri(1), GameSettings.mri(1), "progressBar");
	this.addChild(this._progressBar);
	this._loadingProgress = 0;
};

// ========================= Methods =========================
LoaderBarWidget.prototype.setProgress = function (progress) {
	"use strict";
	this._loadingProgress = progress / 100;
	this._progressBar.scale.x = this._loadingProgress;
};
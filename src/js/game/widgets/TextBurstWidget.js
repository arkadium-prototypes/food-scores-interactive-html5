// ========================= Requirements =========================
var BaseDisplayObjectContainer = require("../objects/BaseDisplayObjectContainer");
var GameSettings = require("../settings/GameSettings");

// ========================= Construction =========================
var TextBurstWidget = function (game, text, fontName, fontSize) {
	"use strict";
	BaseDisplayObjectContainer.call(this, game);
	this._text = text;
	this._fontName = fontName;
	this._fontSize = fontSize;

	this._textLabel = null;
	this._tween = null;
};

// ========================= Prototype =========================
module.exports = TextBurstWidget;
TextBurstWidget.prototype = Object.create(BaseDisplayObjectContainer.prototype);
TextBurstWidget.prototype.constructor = TextBurstWidget;

// ========================= Initialization =========================
TextBurstWidget.prototype.initialize = function () {
	"use strict";
	BaseDisplayObjectContainer.prototype.initialize.call(this);
	this._textLabel = new Phaser.BitmapText(this._game, GameSettings.mri(0), GameSettings.mri(0),
		this._fontName, this._text, this._fontSize, "left");
	this.addChild(this._textLabel);
};

// ========================= Methods =========================
TextBurstWidget.prototype.show = function () {
	"use strict";
	this._tween = this._game.add.tween(this);
	this._tween.onComplete.add(this.handleFinishShowing, this);
	this._tween.to({x:this.x, y:this.y - GameSettings.mri(40), alpha:0}, 800, Phaser.Easing.Linear.None);
	this._tween.start();
};

TextBurstWidget.prototype.handleFinishShowing = function () {
	"use strict";
	this._game.tweens.remove(this._tween);
	this.parent.removeChild(this);
};
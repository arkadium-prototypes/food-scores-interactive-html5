// ========================= Requirements =========================
var GameContextConstants = require("../constants/GameContextConstants");
var HideTransition = require("arkadium-panelmanager").HideTransitionMovement;
var ShowTransition = require("arkadium-panelmanager").ShowTransitionMovement;
var Panel = require("arkadium-panelmanager").Panel;

// ========================= Construction =========================
var BasePanel = function (game, guiBuilder) {
	"use strict";
	Panel.call(this, game, guiBuilder);
	this._eventBus = this._game.gameContext[GameContextConstants.EVENT_BUS];

	this._name = "BasePanel";
	this._initialX = 0;
	this._initialY = 0;
	this._isClosed = false;
};

// ========================= Prototype =========================
module.exports = BasePanel;
BasePanel.prototype = Object.create(Panel.prototype);
BasePanel.prototype.constructor = BasePanel;

// ========================= Getters/Setters =========================
Object.defineProperty(BasePanel, "name", {
	get:function () {
		"use strict";
		return this._name;
	}
});

// ========================= Initialization =========================
BasePanel.prototype.setInitialPosition = function (x, y) {
	"use strict";
	this._initialX = x;
	this._initialY = y;
};

BasePanel.prototype.createShowTransition = function () {
	"use strict";
	var dy = this._game.height * 0.5 + this.height;
	return new ShowTransition(this._game, this, this._initialX, -dy, this._initialX, this._initialY, 300, Phaser.Easing.Back.Out);
};

BasePanel.prototype.createHideTransition = function () {
	"use strict";
	var dy = this._game.height * 0.5 + this.height;
	return new HideTransition(this._game, this, this.x, -dy, 300, Phaser.Easing.Back.In);
};

// ========================= Resize =========================
BasePanel.prototype.onResize = function (aspectRatio) {
	"use strict";
	Panel.prototype.resize.call(this, aspectRatio);
};
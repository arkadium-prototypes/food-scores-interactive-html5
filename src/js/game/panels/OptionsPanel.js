// ========================= Requirements =========================
var BasePanel = require("./BasePanel");
var Button = require("../../core/gui/Button");
var EventBusConstants = require("../constants/EventBusConstants");
var FontConstants = require("../constants/FontConstants");
var GameConstants = require("../constants/GameConstants");
var GameContextConstants = require("../constants/GameContextConstants");
var GameSettings = require("../settings/GameSettings");

// ========================= Construction =========================
var OptionsPanel = function (game, guiBuilder) {
    "use strict";
    BasePanel.call(this, game, guiBuilder);
	this._name = "OptionsPanel";

	// Game contexts
	this._eventBus = this._game.gameContext[GameContextConstants.EVENT_BUS];
	this._textureAtlasHelper = this._game.gameContext[GameContextConstants.TEXTURE_ATLAS_HELPER];

	// Elements
    this._backgroundLayer = null;
    this._contentLayer = null;
};

// ========================= Prototype =========================
module.exports = OptionsPanel;
OptionsPanel.prototype = Object.create(BasePanel.prototype);
OptionsPanel.prototype.constructor = OptionsPanel;

// ========================= Initialization =========================
OptionsPanel.prototype.build = function() {
    "use strict";
	// Exit conditions
	if (this._backgroundLayer) { return;}

	// Create layers
	BasePanel.prototype.build.call(this);
	this._backgroundLayer = new Phaser.Group(this._game, this);
	this.addChild(this._backgroundLayer);
	this._contentLayer = new Phaser.Group(this._game, this);
	this.addChild(this._contentLayer);

	// Add background
	var textureAtlasHelper = this._game.gameContext[GameContextConstants.TEXTURE_ATLAS_HELPER];
	var imageName = "popup_backing";
	this._background = new Phaser.Image(this._game, 0, 0, textureAtlasHelper.getAtlasFor(imageName), imageName);
	this._background.scale.x = this._background.scale.y = 1.5;
	this._backgroundLayer.addChild(this._background);

	// Label
	this._titleLabel = new Phaser.BitmapText(this._game, GameSettings.mri(0), GameSettings.mri(20), FontConstants.SMALL_FONT,
		"Options", 40, "center");
	this._titleLabel.x = (this._background.width - this._titleLabel.textWidth) * 0.5;
	this._titleLabel.y = GameSettings.mri(-1);
	this._contentLayer.addChild(this._titleLabel);

	// Button
	imageName = "btn_close";
	this._closeButton = new Button(this._game, GameConstants.InputPriorities.POPUPS);
	this._closeButton.addBacking(textureAtlasHelper.getAtlasFor(imageName), imageName,
		{tint:"0xFFFFFF"}, {}, {tint:"0xCCCCCC"}, {tint:"0xCCCCCC", offsetY:5});
	this._closeButton.scale.x = this._closeButton.scale.y = 0.6;
	this._closeButton.x = this._background.width - (this._closeButton.width * 0.5);
	this._closeButton.y = this._closeButton.height * 0.8;
	this._closeButton.OnButtonClicked.add(this.handleCloseButtonPressed, this);
	this._contentLayer.addChild(this._closeButton);

	// Set initial positon
	this.setInitialPosition(this._background.width * -0.5, this._background.height * -0.5);
};

OptionsPanel.prototype.handleCloseButtonPressed = function() {
    "use strict";
    if (this._isClosed) {
        return;
    }
    this._isClosed = true;
	this._eventBus.dispatchEvent(EventBusConstants.TopicNames.UI, EventBusConstants.EventNames.UI_OPTIONS_PANEL_CLOSE_REQUESTED);
};

OptionsPanel.prototype.onHided = function() {
    "use strict";
	this.visible = false;
    this._isClosed = false;
    this._eventBus.dispatchEvent(EventBusConstants.TopicNames.UI, EventBusConstants.EventNames.UI_OPTIONS_PANEL_CLOSED);
};
"use strict";

// Run the Phaser patch
var PhaserPatch = require("./core/phaser/PhaserPatch");
window.PhaserGlobal = {stopFocus:true};
var patch = new PhaserPatch();
patch.patch();

// Create the Phaser game
var gameWidth = (window.innerWidth > 0) ? (window.innerWidth) : 640;
var gameHeight = (window.innerHeight > 0) ? (window.innerHeight) : 480;
var game = new Phaser.Game(gameWidth, gameHeight, Phaser.AUTO, "templateGame", null);

// Add all the states (aka scenes)
game.state.add("BootScene", require("./game/scenes/BootScene"));
game.state.add("PreloaderScene", require("./game/scenes/PreloaderScene"));
game.state.add("TitleScene", require("./game/scenes/TitleScene"));
game.state.add("GameScene", require("./game/scenes/GameScene"));
game.state.start("BootScene");
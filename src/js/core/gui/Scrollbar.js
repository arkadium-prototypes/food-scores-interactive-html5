// ================ Requirements ================
var ScrollbarBody = require("./ScrollbarBody");

// ================ Construction ================
var Scrollbar = function (game) {
	"use strict";
	Phaser.Group.call(this, game);
	this._game = game;
	this.inputEnabled = true;
};

// ================ Enums ================
Scrollbar.ScrollStates = {
	NOT_SCROLLING:0, // no scroll for you.
	BAR_BACKING:1, // clicking the scrollbar area
	BAR_BODY:2, // clicking/dragging the physical bar nub
	SCROLL_AREA:3 // clicking/dragging anywhere on the page (that ISN'T the bar, of course)
};
Object.freeze(Scrollbar.ScrollStates);

// ================ Prototype ================
Scrollbar.prototype = Object.create(Phaser.Group.prototype);
Scrollbar.prototype.constructor = Scrollbar;
module.exports = Scrollbar;

// ================ Initialization ================
Scrollbar.prototype.initialize = function (scrollArea, scrollLayers, isVertical, boxSettings, bodySettings) {
	"use strict";
	this._scrollAreaRef = scrollArea;
	this._scrollLayersRef = scrollLayers;
	this._isVertical = isVertical;

	// Point properties
	this._barBodyDimensions = new Phaser.Point(0, 0); // barBody's dimensions if there's no squash applied to the barBody. Almost definitely won't have an effect, but I just think it's neater to have this as a variable.
	this._boxDimensions = new Phaser.Point(0, 0); // dimensions of the scrollbar's backing area
	this._clickOffset = new Phaser.Point(0, 0);
	this._inputPos = new Phaser.Point(0, 0);
	this._inputDown = new Phaser.Point(0, 0); // the position of the input WHEN I clicked down!
	this._minBarBodyDimensions = new Phaser.Point(0, 0);
	this._scrollLayerDimensions = new Phaser.Point(0, 0); // set by whomever owns me, whenever the screen is resized or the height is somehow changed.

	// Other properties
	this._isInputDown = false;
	this._isScrolled = false;
	this._numFramesInputIsDown = 0;
	this.reset(); // Reset properties

	// Add scrollbar backing
	if (boxSettings !== undefined) {
		this._backing = new Phaser.Image(this._game, 0, 0, boxSettings.atlas, boxSettings.image);
		this._backing.inputEnabled = true;
		if (boxSettings.tint !== undefined) { this._backing.tint = boxSettings.tint; }
		if (boxSettings.alpha !== undefined) { this._backing.alpha = boxSettings.alpha; }
		this.addChild(this._backing);
	}

	// Add scrollbar body
	if (bodySettings !== undefined) {
		this._barBody = new ScrollbarBody(this._game, isVertical, bodySettings);
		this._barBody.inputEnabled = true;
		this.addChild(this._barBody);
	}
};


Scrollbar.prototype.reset = function () {
	"use strict";
	this._allowOvershooting = true; // we allow scrolling velocity to overshoot the target
	this._easingSpeed = 0.25; // How much easing we should do during the scroll, higher the number = faster the scroll
	this._clickOffset.x = 0; // used for BOTH Bar Body and Whole Page scrolling. This is set to the difference between the bar's/the page's x and the input x.
	this._clickOffset.y = 0; // used for BOTH Bar Body and Whole Page scrolling. This is set to the difference between the bar's/the page's y and the input y.
	this._framesSinceStoppedScrolling = 0; // how many frames it's been since I've been NOT scrolling *AND* my velocity's been 0.
	this._scrollState = Scrollbar.ScrollStates.NOT_SCROLLING;
	this._scrollPos = 0;
	this._scrollTarget = 0; // from 0 to 1.
	this._scrollTargetVel = 0; // this is set when we call setScrollTargetTo and friction is applied to it every frame.
};

// ================ Updating Transform ================
Scrollbar.prototype.updatePositionAndSize = function (x, y, boxWidth, boxHeight) {
	"use strict";
	// Set parameters
	this.x = x;
	this.y = y;
	this._boxDimensions.x = boxWidth;
	this._boxDimensions.y = boxHeight;

	// Resize backing
	this._backing.width = this._boxDimensions.x;
	this._backing.height = this._boxDimensions.y;	

	// Resize body
	this._barBodyDimensions.x = this._boxDimensions.x * this._boxDimensions.x / this._scrollLayerDimensions.x;
	this._barBodyDimensions.y = this._boxDimensions.y * this._boxDimensions.y / this._scrollLayerDimensions.y;
	if (this._barBodyDimensions.x < this._boxDimensions.x*0.1) { this._barBodyDimensions.x = this._boxDimensions.x*0.1; }
	if (this._barBodyDimensions.y < this._boxDimensions.y*0.1) { this._barBodyDimensions.y = this._boxDimensions.y*0.1; }
	this._minBarBodyDimensions.x = this._boxDimensions.x*0.03;
	this._minBarBodyDimensions.y = this._boxDimensions.y*0.03;
	this.updateBarBody();

	// Hide scrollbar if items can be scrolled
	if (this._isVertical) { this.visible = this._barBodyDimensions.y < this._boxDimensions.y; }
	else { this.visible = this._barBodyDimensions.x < this._boxDimensions.x; }
};

Scrollbar.prototype.updateBarBody = function () {
	"use strict";
	// If barBody is too far off UP or DOWN, then shrink it a bunch!
	if (this._isVertical) {
		var bodyWidth = this._boxDimensions.x*0.95;
		var bodyHeight = this._barBodyDimensions.y;
		var bodyY = this._scrollPos * (this._boxDimensions.y-bodyHeight);

		var barBodyMinY = 0;
		var barBodyMaxY = this._boxDimensions.y-bodyHeight;
		if (bodyY < barBodyMinY) {
			bodyHeight += (bodyY-barBodyMinY);
			bodyHeight = Math.max(this._minBarBodyDimensions.y, bodyHeight);
			bodyY = barBodyMinY;
		}
		else if (bodyY > barBodyMaxY) {
			var heightShrink = -(bodyY-barBodyMaxY);
			bodyHeight += heightShrink;
			bodyHeight = Math.max(this._minBarBodyDimensions.y, bodyHeight);
			bodyY = this._boxDimensions.y - bodyHeight;
		}
		this._barBody.updatePositionAndSize(this._boxDimensions.x*0.5, bodyY+(bodyHeight*0.5), bodyWidth, bodyHeight);
	}

	// If barBody is too far off LEFT or RIGHT, then shrink it a bunch!
	else {
		var bodyWidth = this._barBodyDimensions.x;
		var bodyHeight = this._boxDimensions.y*0.95;
		var bodyX = this._scrollPos * (this._boxDimensions.x-bodyWidth);

		var barBodyMinX = 0;
		var barBodyMaxX = this._boxDimensions.x-bodyWidth;
		if (bodyX < barBodyMinX) {
			bodyWidth += (bodyX-barBodyMinX);
			bodyWidth = Math.max(this._minBarBodyDimensions.x, bodyWidth);
			bodyX = barBodyMinX;
		}
		else if (bodyX > barBodyMaxX) {
			var widthShrink = -(bodyX-barBodyMaxX);
			bodyWidth += widthShrink;
			bodyWidth = Math.max(this._minBarBodyDimensions.x, bodyWidth);
			bodyX = this._boxDimensions.x - bodyWidth;
		}
		this._barBody.updatePositionAndSize(bodyX+(bodyWidth*0.5), this._boxDimensions.y*0.5, bodyWidth, bodyHeight);
	}
};


// ================ Input Events ================
Scrollbar.prototype.onStageInputDown = function () {
	"use strict";
	this.updateInputPos();

	// Clicking/dragging BAR DIRECTLY.
	if (this.checkIfBarHasSpaceToMove()) {
		if (this.isPointOverScrollbarBody(this._inputPos.x, this._inputPos.y)) {
			this.beginScrolling(Scrollbar.ScrollStates.BAR_BODY, true);
		}
		else if (this.isPointOverScrollbarBacking(this._inputPos.x, this._inputPos.y)) {
			this.beginScrolling(Scrollbar.ScrollStates.BAR_BACKING, false);
		}
		// Clicking/dragging WHOLE SCROLL LAYER.
		else if (this.isPointOverScrollArea(this._inputPos.x, this._inputPos.y)) {
			this._isInputDown = true;
			this._inputDown.x = this._inputPos.x; // Cache input coord
			this._inputDown.y = this._inputPos.y; // Cache input coord
		}
	}
};
Scrollbar.prototype.onStageInputUp = function () {
	"use strict";
	this._isInputDown = false;
	this._scrollState = Scrollbar.ScrollStates.NOT_SCROLLING;
	this._barBody.updateVisualStatus(false);
};

Scrollbar.prototype.onStageInputScrollWheel = function() {
	"use strict";
	this.updateInputPos();
	if (this.isPointOverScrollArea(this._inputPos.x, this._inputPos.y)) {
		if (this.checkIfBarHasSpaceToMove()) {
			var direction = Math.sign(this._game.input.mouse.wheelDelta);
			var distance = (this._isVertical) ? (this._boxDimensions.y / this._scrollLayerDimensions.y) : (this._boxDimensions.x / this._scrollLayerDimensions.x);
			this.setScrollTargetTo(this._scrollTarget - (direction * distance * 0.15));
		}
	}
};

// ================ Input Detection ================
Scrollbar.prototype.isPointOverScrollArea = function (x ,y) {
	"use strict";
	return (x>this._scrollAreaRef.x && x<this._scrollAreaRef.x+this._scrollAreaRef.width) &&
		(y>this._scrollAreaRef.y && y<this._scrollAreaRef.y+this._scrollAreaRef.height);
};
Scrollbar.prototype.isPointOverScrollbarBacking = function (x ,y) {
	"use strict";
	return (x>this.x && x<this.x+this._boxDimensions.x) && (y>this.y && y<this.y+this._boxDimensions.y);
};
Scrollbar.prototype.isPointOverScrollbarBody = function (x ,y) {
	"use strict";
	return (x>this.x && x<this.x+this._boxDimensions.x+this._barBody.width) &&
		(y>this.y+this._barBody.y-this._barBody.height*0.5 && y<this.y+this._barBody.y+this._barBody.height*0.5);
};

Scrollbar.prototype.checkIfBarHasSpaceToMove = function() {
	"use strict";
	// If my scrollbar is the size of the whole thing, I can't scroll anywhere, so don't let input do anything. Otherwise, things bug out a little.
	return (this._isVertical) ? (this._barBodyDimensions.y < this._boxDimensions.y) : (this._barBodyDimensions.x < this._boxDimensions.x);
};


// ================ Update ================
Scrollbar.prototype.update = function () {
	"use strict";
	// Set input locations
	this.updateInputPos();

	// Input is DOWN and we're NOT scrolling...!
	if (this._isInputDown && this._scrollState===Scrollbar.ScrollStates.NOT_SCROLLING) {
		// We've moved far enough from source touch to scroll, OR our velocity is NOT 0 (so we can halt the scrolling by tapping back down while it's moving)?...
		if ((this._isVertical && Math.abs(this._inputPos.y-this._inputDown.y) > (this._barBodyDimensions.y * 0.15)) ||
			(!this._isVertical && Math.abs(this._inputPos.x-this._inputDown.x) > (this._barBodyDimensions.x * 0.15)) ||
			(this._scrollTargetVel!==0)) {
			this.beginScrolling(Scrollbar.ScrollStates.SCROLL_AREA, true);
		}
	}

	// ---- Scrolling! ----
	if (this._scrollState !== Scrollbar.ScrollStates.NOT_SCROLLING) {
		// Bar Backing
		if (this._scrollState === Scrollbar.ScrollStates.BAR_BACKING) {
			// Don't immediately move until player has held down for a few frames
			this._numFramesInputIsDown++;
			if (this._numFramesInputIsDown > 3) {
				this.moveScrollTargetFromInput(this._inputPos);
			}
		}
		// Bar Body
		else if (this._scrollState === Scrollbar.ScrollStates.BAR_BODY) {
			this.setScrollTargetFromInput(this._inputPos);
		}
		// Whole Page
		else if (this._scrollState === Scrollbar.ScrollStates.SCROLL_AREA) {
			this.setScrollTargetFromLayerPos(this._inputPos);
		}
	}

	// Update _framesSinceStoppedScrolling
	if (this._scrollState===Scrollbar.ScrollStates.NOT_SCROLLING && this._scrollTargetVel===0) { this._framesSinceStoppedScrolling ++; }
	else { this._framesSinceStoppedScrolling = 0; }

	// Apply "GRAVITY" - Always ease scrollYTarget into bounds.
	// Note: This is one of TWO applied gravities; this one is always active, and it's to firmly nudge scrollYTarget back into bounds.
	if (this._scrollTarget < 0) { this._scrollTarget += (0 - this._scrollTarget) / 6; }
	else if (this._scrollTarget > 1) { this._scrollTarget += (1 - this._scrollTarget) / 6; }

	// If I'm not scrolling me one way or another, apply VELOCITY!
	if (this._scrollState === Scrollbar.ScrollStates.NOT_SCROLLING) {
		if (this._allowOvershooting) { this._scrollTarget += this._scrollTargetVel; } // apply velocity if we're easing
		this._scrollTargetVel *= 0.65; // add friction so we slow down
		if (Math.abs(this._scrollTargetVel) < (this._barBodyDimensions.y/5000)) { this._scrollTargetVel = 0; } // If I'm *almost* stopped, go ahead and stop me early.
	}


	// Store previous scrollY position
	var previousScrollPos = this._scrollPos;
	// Whole Page scrolling?? SET _scrollY exactly to its target.
	if (this._scrollState === Scrollbar.ScrollStates.SCROLL_AREA) {
		this._scrollPos = this._scrollTarget;
	}
	// NOT Whole Page scrolling?? EASE _scrollY to its target.
	else {
		this._scrollPos += (this._scrollTarget-this._scrollPos) * this._easingSpeed;
	}

	// If I AM scrolling somehow, set my velocity to be how far I've just moved!
	if (this._scrollState !== Scrollbar.ScrollStates.NOT_SCROLLING) {
		this._scrollTargetVel = (this._scrollPos - previousScrollPos);
	}

	// Update position/scale of bar
	this.updateBarBody();

	// FINALLY, update the position of our layer reference!
	for (var i=0, len=this._scrollLayersRef.length; i<len; ++i) {
		if (this._isVertical) { this._scrollLayersRef[i].y = -this._scrollPos * (this._scrollLayerDimensions.y - this._boxDimensions.y); }
		else { this._scrollLayersRef[i].x = -this._scrollPos * (this._scrollLayerDimensions.x-this._boxDimensions.x); }
	}
};
Scrollbar.prototype.updateInputPos = function () {
	"use strict";
	if (!this.parent) { return; } // Exit condition
	this._inputPos = this._game.input.getLocalPosition(this.parent, this._game.input.activePointer);
};

// ================ Scrolling Functions ================
Scrollbar.prototype.beginScrolling = function (scrollState, allowOvershooting) {
	"use strict";
	// Set _scrollState and reset velocity to 0.
	this._isScrolled = true;
	this._scrollState = scrollState;
	this._scrollTargetVel = 0;
	this._allowOvershooting = allowOvershooting;

	// Bar Backing
	if (this._scrollState === Scrollbar.ScrollStates.BAR_BACKING) {
		this._numFramesInputIsDown = 0;
		this.moveScrollTargetFromInput(this._inputPos);
	}
	// Bar Body
	else if (this._scrollState === Scrollbar.ScrollStates.BAR_BODY) {
		this._clickOffset.x = this._inputPos.x - this.x - this._barBody.x + (this._barBodyDimensions.x * 0.5);
		if (this._clickOffset.x < 0 || this._clickOffset.x > this._barBodyDimensions.x) {
			this._clickOffset.x = this._barBodyDimensions.x * 0.5;
		}
		this._clickOffset.x += this.x;
		this._clickOffset.y = this._inputPos.y - this.y - this._barBody.y + (this._barBodyDimensions.y * 0.5);
		if (this._clickOffset.y < 0 || this._clickOffset.y > this._barBodyDimensions.y) {
			this._clickOffset.y = this._barBodyDimensions.y * 0.5;
		}
		this._clickOffset.y += this.y;

		this.setScrollTargetFromInput(this._inputPos);
		this._barBody.updateVisualStatus(true);
	}
	// Whole Page
	else {
		this._clickOffset.x = this._inputPos.x - this._scrollLayersRef[0].x;
		this._clickOffset.y = this._inputPos.y - this._scrollLayersRef[0].y;
	}
};

Scrollbar.prototype.moveScrollTargetFromInput = function (input) {
	"use strict";
	if (this._isVertical) {
		var distance = (this._boxDimensions.y / this._scrollLayerDimensions.y) * 0.5;

		// If click is above bar body, then move it upwards
		if (input.y < this._barBody.y - (this._barBody.height * 0.25)) {
			this.setScrollTargetTo(this._scrollTarget - distance);
		}
		// If click is below bar body, then move it downwards
		else if (input.y > this._barBody.y + (this._barBody.height * 0.25)) {
			this.setScrollTargetTo(this._scrollTarget + distance);
		}
		// If bar body moved onto the point, then start scrolling it as bar body
		else {
			this.beginScrolling(Scrollbar.ScrollStates.BAR_BODY, true);
		}
	}
	else {
		var distance = (this._boxDimensions.x / this._scrollLayerDimensions.x) * 0.5;

		// If click is above bar body, then move it upwards
		if (input.x < this._barBody.x - (this._barBody.width * 0.25)) {
			this.setScrollTargetTo(this._scrollTarget - distance);
		}
		// If click is below bar body, then move it downwards
		else if (input.x > this._barBody.x + (this._barBody.width * 0.25)) {
			this.setScrollTargetTo(this._scrollTarget + distance);
		}
		// If bar body moved onto the point, then start scrolling it as bar body
		else {
			this.beginScrolling(Scrollbar.ScrollStates.BAR_BODY, true);
		}
	}

};
Scrollbar.prototype.setScrollTargetFromInput = function (input) {
	"use strict";
	if (this._isVertical) { this.setScrollTargetTo((input.y - this._clickOffset.y) / (this._boxDimensions.y - this._barBody.height)); }
	else { this.setScrollTargetTo((input.x - this._clickOffset.x) / (this._boxDimensions.x - this._barBody.width)); }
	this._easingSpeed = 0.25;
	this._scrollTargetVel *= 0.3; // Really mute the velocity, here.
};
Scrollbar.prototype.setScrollTargetFromLayerPos = function (layer) {
	"use strict";
	if (this._isVertical) { this.setScrollTargetTo(-(layer.y-this._clickOffset.y) / (this._scrollLayerDimensions.y-this._boxDimensions.y)); }
	else { this.setScrollTargetTo(-(layer.x-this._clickOffset.x) / (this._scrollLayerDimensions.x-this._boxDimensions.x)); }
};
Scrollbar.prototype.setScrollTargetFromItemPos = function (itemXPos, itemYPos, itemWidth, itemHeight) {
	"use strict";
	// If scrollbar is visible, then scroll to appropriate position
	this._allowOvershooting = false;
	this._easingSpeed = 0.5;
	this._scrollTarget = 0;

	if (this._isVertical) { this.setScrollTargetTo(itemYPos / (this._scrollLayerDimensions.y - itemHeight * 0.5)); }
	else { this.setScrollTargetTo(itemXPos / (this._scrollLayerDimensions.x - itemWidth)); }
};

/** Change scrollYTarget AND update its velocity, too. */
Scrollbar.prototype.setScrollTargetTo = function (scrollTarget) {
	"use strict";
	// Apply "GRAVITY" to this requested target! This is similar to Apple's mobile scrolling standards, where you can scroll off the page, but not by a lot.
	// Note: This is one of TWO applied gravities; this one is for being allowed to set scrollTarget to something in the first place (so it's stronger).
	if (scrollTarget < 0) { scrollTarget += (0-scrollTarget) / 2; } // ease scrollTarget to 0.
	else if (scrollTarget > 1) { scrollTarget += (1-scrollTarget) / 2; } // ease scrollTarget to 1.
	// Set the velocity to the difference between the old/new values
	this._scrollTargetVel = (scrollTarget - this._scrollTarget);
	// And, obviously, set the value.
	this._scrollTarget = scrollTarget;
};

// ================ Destruction ================
Scrollbar.prototype.destroy = function () {
	"use strict";
	this._barBody.destroy();
};

// ================ Properties ================
Object.defineProperty(Scrollbar.prototype, "BoxWidth", { get: function () { "use strict"; return this._boxDimensions.x; } });
Object.defineProperty(Scrollbar.prototype, "BoxHeight", { get: function () { "use strict"; return this._boxDimensions.y; } });
Object.defineProperty(Scrollbar.prototype, "FramesSinceStoppedScrolling", { get: function () { "use strict"; return this._framesSinceStoppedScrolling; } });
Object.defineProperty(Scrollbar.prototype, "InputPos", { get: function () { "use strict"; return this._inputPos; } });
Object.defineProperty(Scrollbar.prototype, "IsInputDown", { get: function () { "use strict"; return this._isInputDown; } });
Object.defineProperty(Scrollbar.prototype, "IsScrolled", {
	get: function () { "use strict"; return this._isScrolled; },
	set: function (value) { "use strict"; this._isScrolled = value; }
});
Object.defineProperty(Scrollbar.prototype, "ScrollLayerWidth", { set: function (value) { "use strict"; this._scrollLayerDimensions.x = value; } });
Object.defineProperty(Scrollbar.prototype, "ScrollLayerHeight", { set: function (value) { "use strict"; this._scrollLayerDimensions.y = value; } });
/**
 * Created by dor on 7/1/2016.
 */
// ========================= Requirements =========================
var NineSliceImageBody = require("./NineSliceImageBody");

// =============== Construction ===============
var NineSliceImage = function(game, inputPriorityId) {
	"use strict";
	Phaser.Group.call(this, game, null);
	this._game = game;
	this._inputPriorityId = inputPriorityId;
	this._sliceType = 0;

	// Bodies
	this._top = null;
	this._middle = null;
	this._bottom = null;
	this._left = null;
	this._right = null;
	this._corners = [];
};

// =============== Registration ===============
NineSliceImage.prototype = Object.create(Phaser.Group.prototype);
NineSliceImage.prototype.constructor = NineSliceImage;
module.exports = NineSliceImage;

// =============== Enums ===============
NineSliceImage.Types = {
	UNDEFINED: 0,
	ONE_SLICE: 1,
	THREE_SLICE_VERTICAL: 2,
	THREE_SLICE_HORIZONTAL: 3,
	NINE_SLICE: 4
};
Object.freeze(NineSliceImage.Types);

// =============== Slices ===============
NineSliceImage.prototype.addTop = function (atlas, image, settings) {
	"use strict";
	this._top = new NineSliceImageBody(this._game, this._inputPriorityId, atlas, image, settings);
	this.addChild(this._top);
};
NineSliceImage.prototype.addMiddle = function (atlas, image, settings) {
	"use strict";
	this._middle = new NineSliceImageBody(this._game, this._inputPriorityId, atlas, image, settings);
	this.addChild(this._middle);
};
NineSliceImage.prototype.addBottom = function (atlas, image, settings) {
	"use strict";
	this._bottom = new NineSliceImageBody(this._game, this._inputPriorityId, atlas, image, settings);
	this.addChild(this._bottom);
};
NineSliceImage.prototype.addLeft = function (atlas, image, settings) {
	"use strict";
	this._left = new NineSliceImageBody(this._game, this._inputPriorityId, atlas, image, settings);
	this.addChild(this._left);
};
NineSliceImage.prototype.addRight = function (atlas, image, settings) {
	"use strict";
	this._right = new NineSliceImageBody(this._game, this._inputPriorityId, atlas, image, settings);
	this.addChild(this._right);
};
NineSliceImage.prototype.addCorners = function (atlas, image, settings) {
	"use strict";
	for (var i=0; i<4; ++i) {
		var newCorner = new NineSliceImageBody(this._game, this._inputPriorityId, atlas, image, settings);
		this.addChild(newCorner);
		this._corners.push(newCorner);
	}
};

// =============== Slices ===============
NineSliceImage.prototype.initializeType = function() {
	"use strict";
	if (this._corners.length > 0) { this._sliceType = NineSliceImage.Types.NINE_SLICE; }
	else if (this._top && this._middle && this._bottom) { this._sliceType = NineSliceImage.Types.THREE_SLICE_VERTICAL; }
	else if (this._left && this._middle && this._right) { this._sliceType = NineSliceImage.Types.THREE_SLICE_HORIZONTAL; }
	else if (this._middle) { this._sliceType = NineSliceImage.Types.ONE_SLICE; }
};

// =============== Resize ===============
NineSliceImage.prototype.onResize = function (width, height) {
	"use strict";
	var IMAGE_PADDING = 1; // Add some additional padding to the tiles, to remove seams
	switch (this._sliceType) {
		// Just resize the one slice based on the dimensions given
		case NineSliceImage.Types.ONE_SLICE:
			this._middle.onResize(width, height);
			break;

		// Resize top and bottom, then fit the middle in the remaining space
		case NineSliceImage.Types.THREE_SLICE_VERTICAL:
			// Resize the slices
			this._top.onResize(width, height);
			this._bottom.onResize(width, height);
			this._middle.onResize(width, height - (this._top.height + this._bottom.height) + (IMAGE_PADDING * 2));

			// Position the slices
			this._top.x = width * 0.5;
			this._top.y = (this._top.height * 0.5);
			this._middle.x = width * 0.5;
			this._middle.y = this._top.y + (this._top.height + this._middle.height) * 0.5 - IMAGE_PADDING;
			this._bottom.x = width * 0.5;
			this._bottom.y = this._middle.y + (this._middle.height + this._bottom.height) * 0.5 - IMAGE_PADDING;
			break;

		// Resize left and right, then fit the middle in the remaining space
		case NineSliceImage.Types.THREE_SLICE_HORIZONTAL:
			// Resize the slices
			this._left.onResize(width, height);
			this._right.onResize(width, height);
			this._middle.onResize(width - (this._left.width + this._right.width) + (IMAGE_PADDING * 2), height);

			// Position the slices
			this._left.x = (this._left.width * 0.5);
			this._left.y = height * 0.5;
			this._middle.x = this._left.x + (this._left.width + this._middle.width) * 0.5 - IMAGE_PADDING;
			this._middle.y = height * 0.5;
			this._right.x = this._middle.x + (this._middle.width + this._right.width) * 0.5 - IMAGE_PADDING;
			this._right.y = height * 0.5;
			break;

		// Position the outer frame, then fit the middle in the remaining space
		case NineSliceImage.Types.NINE_SLICE:
			// TODO: Code this
			break;
	}
};

// =============== Input ===============
NineSliceImage.prototype.enableInput = function(value) {
	"use strict";
	if (this._top) { this._top.inputEnabled = value; }
	if (this._middle) { this._middle.inputEnabled = value; }
	if (this._bottom) { this._bottom.inputEnabled = value; }
	if (this._left) { this._left.inputEnabled = value; }
	if (this._right) { this._right.inputEnabled = value; }
};

// =============== Destruction ===============
NineSliceImage.prototype.destroy = function () {
	"use strict";
	Phaser.Group.prototype.destroy.call(this);
};
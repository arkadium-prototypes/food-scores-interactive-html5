// ========================= Requirements =========================
var Scrollbar = require("./Scrollbar");

// ========================= Construction =========================
var ListBox = function (game, isVertical, priorityID) {
	"use strict";
	Phaser.Group.call(this, game);
	this._game = game;
	this._isVertical = isVertical;
	this._priorityID = priorityID;
	this.OnSelectItem = new Phaser.Signal();

	// Variables
	this._items = [];
	this._layers = [];
	this._inputEnabled = true;
	this._itemBufferSpace = 0;

	// Mask
	this._customHitArea = {x:0, y:0, width:0, height:0};
	this._customMask = new Phaser.Graphics(this._game, 0, 0);
	this._customMask.beginFill(0xffffff);
	this._customMask.drawRect(0, 0, 100, 100);
	this.mask = this._customMask;

	// Add event listeners
	this._game.input.onDown.add(this.onStageInputDown, this);
	this._game.input.onUp.add(this.onStageInputUp, this);
};

// ========================= Prototype =========================
module.exports = ListBox;
ListBox.prototype = Object.create(Phaser.Group.prototype);
ListBox.prototype.constructor = ListBox;

// ========================= Initialization =========================
ListBox.prototype.addBackground = function (atlas, image, color, alpha) {
	"use strict";
	this._background = new Phaser.Image(this._game, 0, 0, atlas, image);
	this._background.alpha = alpha;
	this._background.inputEnabled = true;
	this._background.input.useHandCursor = true; // Show the hand cursor only when it's over the background
	this._background.input.priorityID = this._priorityID;
	this._background.tint = color;
	this.addChild(this._background);
};
ListBox.prototype.addLayers = function (numLayers) {
	"use strict";
	for (var i= 0; i<numLayers; ++i) {
		var newLayer = new Phaser.Group(this._game);
		this.addChild(newLayer);
		this._layers.push(newLayer);
	}
};
ListBox.prototype.addScrollbar = function (boxSizeRatio, boxSettings, bodySettings) {
	"use strict";
	this._scrollbarSizeRatio = boxSizeRatio;
	this._scrollbar = new Scrollbar(this._game);
	this._scrollbar.initialize(this._background, this._layers, this._isVertical, boxSettings, bodySettings);
	this.addChild(this._scrollbar);
};

// ========================= Resize =========================
ListBox.prototype.onResize = function (width, height) {
	"use strict";
	// Resize background
	this._background.width = width;
	this._background.height = height;

	// Resize items
	for (var i=0, len=this._items.length; i<len; ++i) {
		this._items[i].onResize(width, height);
	}
	this.positionItems(); // Reposition the items after resizing
	this.resizeScrollbar(); // Resize the scroll bar to match the items
};

ListBox.prototype.positionItems = function () {
	"use strict";
	var itemPos = 0;
	for (var i=0, len=this._items.length; i<len; ++i) {
		// Only layout visible items
		if (this._items[i].visible) {
			if (this._isVertical) {
				this._items[i].y = itemPos;
				itemPos += this._items[i].height + this._itemBufferSpace;
			}
			else {
				this._items[i].x = itemPos;
				itemPos += this._items[i].width + this._itemBufferSpace;
			}
		}
		// If invisible, then place it at the origin
		else {
			this._items[i].x = 0;
			this._items[i].y = 0;
		}
	}
	this.centerItems(itemPos); // Center items if there's a lot of empty space
};

ListBox.prototype.centerItems = function (itemPos) {
	"use strict";
	var offset, i, len;
	// If there are less items than can fill the space, then center it
	if (this._isVertical && itemPos < this._background.height) {
		offset = (this._background.height - itemPos) * 0.5;
		for (i=0, len=this._items.length; i<len; ++i) {
			if (this._items[i].visible) {
				this._items[i].y += offset;
			}
		}
	}
	else if (!this._isVertical && itemPos < this._background.width) {
		offset = (this._background.width - itemPos) * 0.5;
		for (i=0, len=this._items.length; i<len; ++i) {
			if (this._items[i].visible) {
				this._items[i].x += offset;
			}
		}
	}
};

ListBox.prototype.resizeScrollbar = function () {
	"use strict";
	// Set variables
	this._scrollbar.ScrollLayerWidth = this._layers[0].width;
	this._scrollbar.ScrollLayerHeight = this._layers[0].height;
	var width = this._background.width;
	var height = this._background.height;

	// Resize scrollbar
	if (this._isVertical) {
		var scrollBarWidth = width * this._scrollbarSizeRatio;
		this._scrollbar.updatePositionAndSize(width-scrollBarWidth, 0, scrollBarWidth, height);
	}
	else {
		var scrollBarHeight = height * this._scrollbarSizeRatio;
		this._scrollbar.updatePositionAndSize(0, height-scrollBarHeight, width, scrollBarHeight);
	}
};

ListBox.prototype.resizeMask = function (x, y) {
	"use strict";
	// #caveat: Resizing the mask MUST be done after all other elements are resized, positioned, and updated
	this._customMask.clear();
	this._customMask.beginFill(0xffffff);
	this._customMask.drawRect(this.x + x, this.y + y, this._background.width, this._background.height);

	// Update the hit area to match the mask
	this._customHitArea.x = this._customMask.x;
	this._customHitArea.y = this._customMask.y;
	this._customHitArea.width = this._customMask.width;
	this._customHitArea.height = this._customMask.height;
};


// ========================= Properties =========================
Object.defineProperty(ListBox.prototype, "Background", {
	get: function () { "use strict"; return this._background; }
});

Object.defineProperty(ListBox.prototype, "ItemBufferSpace", {
	set: function (value) { "use strict"; this._itemBufferSpace = value; }
});
Object.defineProperty(ListBox.prototype, "Items", {
	get: function () { "use strict"; return this._items; }
});
Object.defineProperty(ListBox.prototype, "ItemWidth", {
	get: function () { "use strict"; return (this._items.length>0) ? (this._items[0].width) : 0; }
});
Object.defineProperty(ListBox.prototype, "ItemHeight", {
	get: function () { "use strict"; return (this._items.length>0) ? (this._items[0].height) : 0; }
});

Object.defineProperty(ListBox.prototype, "Layers", {
	get: function () { "use strict"; return this._layers; }
});
Object.defineProperty(ListBox.prototype, "LayerWidth", {
	get: function () { "use strict"; return (this._layers.length>0) ? (this._layers[0].width) : 0; }
});
Object.defineProperty(ListBox.prototype, "LayerHeight", {
	get: function () { "use strict"; return (this._layers.length>0) ? (this._layers[0].height) : 0; }
});

Object.defineProperty(ListBox.prototype, "ScrollBarWidth", {
	get: function () { "use strict"; return this._scrollbar.width; }
});
Object.defineProperty(ListBox.prototype, "ScrollBarHeight", {
	get: function () { "use strict"; return this._scrollbar.height; }
});

// ========================= Items =========================
ListBox.prototype.addItem = function (item) {
	"use strict";
	this._items.push(item);
	//item.autoCull = false; // Don't use Phaser's built-in auto-culling, due to its low performance
};

ListBox.prototype.scrollToItem = function (item) {
	"use strict";
	// Check whether we should scroll
	if (this._layers &&
		// If horizontal, and the item is already visible
		((this._layers[0].x + item.x) > 0 && (this._layers[0].x + item.x + item.width) < this._scrollbar.BoxWidth) ||
		// Or if vertical, and the item is already visible
		((this._layers[0].y + item.y) > 0 && (this._layers[0].y + item.y + item.height) < this._scrollbar.BoxHeight)) {
		return;
	}
	
	// Scroll to the item
	this._scrollbar.setScrollTargetFromItemPos(item.x, item.y, item.width, item.height);
};

// ========================= Item Selection =========================
ListBox.prototype.setHitArea = function (x, y, width, height) {
	"use strict";
	if (this._customHitArea) {
		this._customHitArea.x = x;
		this._customHitArea.y = y;
		this._customHitArea.width = width;
		this._customHitArea.height = height;
	}
};
ListBox.prototype.checkIfItemIsSelected = function () {
	"use strict";
	if (!this._scrollbar.IsScrolled) { // If user didn't scroll, then register it as a click
		if (this._scrollbar.InputPos.x >= this._customHitArea.x && this._scrollbar.InputPos.x <= this._customHitArea.width &&
			this._scrollbar.InputPos.y >= this._customHitArea.y && this._scrollbar.InputPos.y <= this._customHitArea.height) { // Only allow selection of items within the mask
			var touchedPos, i, len;

			// Check for selected item in vertical list box
			if (this._isVertical) {
				touchedPos = this._scrollbar.InputPos.y - this._layers[0].y; // Find the touched position relative to the base layer
				for (i=0, len=this._items.length; i < len; ++i) { // Iterate through items to find the one that was touched
					if (this._items[i].visible && (touchedPos >= this._items[i].y) && (touchedPos <= this._items[i].y + this._items[i].height)) {
						this.OnSelectItem.dispatch(this._items[i], i); // Dispatch an event if we found the touched item
						break; // Stop iterating after we've found the culprit
					}
				}
			}

			// Otherwise, check for selected item in horizontal list box
			else {
				touchedPos = this._scrollbar.InputPos.x - this._layers[0].x; // Find the touched position relative to the base layer
				for (i=0, len=this._items.length; i < len; ++i) { // Iterate through items to find the one that was touched
					if (this._items[i].visible && (touchedPos >= this._items[i].x) && (touchedPos <= this._items[i].x + this._items[i].width)) {
						this.OnSelectItem.dispatch(this._items[i], i); // Dispatch an event if we found the touched item
						break; // Stop iterating after we've found the culprit
					}
				}
			}
		}
	}
};

// ========================= Input =========================
ListBox.prototype.onStageInputDown = function () {
	"use strict";
	if (!this._inputEnabled) { return; } // Exit if no input is enabled
	this._scrollbar.IsScrolled = false;
	this._scrollbar.onStageInputDown();
};
ListBox.prototype.onStageInputUp = function () {
	"use strict";
	if (!this._inputEnabled) { return; } // Exit if no input is enabled
	// If we started and ended a touch on this listbox, then check if we selected an item
	if (this._scrollbar.IsInputDown || !this._scrollbar.visible) {
		this.checkIfItemIsSelected();
	}
	this._scrollbar.onStageInputUp();
};
ListBox.prototype.onStageInputScrollWheel = function() {
	"use strict";
	if (!this._inputEnabled) { return; } // Exit if no input is enabled
	if (this._scrollbar) { this._scrollbar.onStageInputScrollWheel(); }
};

// ========================= Input =========================
ListBox.prototype.enableInput = function (value) {
	"use strict";
	this._inputEnabled = value;
};

// ========================= Destruction =========================
ListBox.prototype.destroy = function () {
	"use strict";
	// Remove event listeners
	this._game.input.onDown.remove(this.onStageInputDown, this);
	this._game.input.onUp.remove(this.onStageInputUp, this);

	// Destroy items
	for (var i=0, len=this._items.length; i < len; ++i) {
		this._items[i].destroy();
	}
	this._scrollbar.destroy();

	// Super destroy
	Phaser.Group.prototype.destroy.call(this);
};
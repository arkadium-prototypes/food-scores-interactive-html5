// ========================= Requirements =========================
var ButtonBody = 	require("./ButtonBody");
var TextDisplay = 	require("./TextDisplay");
var DisplayUtils = 	require("../utils/DisplayUtils");

// ========================= Construction =========================
var Button = function (game, inputPriority) {
	"use strict";
	// Construct
	Phaser.Group.call(this, game, null);
	this._game = game;
	this._inputPriority = inputPriority || 0;

	// Initialize variables
	this._buttonBodies = [];
	this._customHitArea = null;
	this._customHitAreaRatios = [];
	this._touchableItem = null;
	this.OnButtonClicked = new Phaser.Signal();

	// Initialize Booleans
	this._isInputDown = false; // Both these pertain exclusively to me as a button (so not globally input is down)
	this._isInputOver = false; // Both these pertain exclusively to me as a button (so not globally input is down)
	this._iconAlignment = "left"; // By default, align the icon to the left of the label
};

// ========================= Prototype =========================
Button.prototype = Object.create(Phaser.Group.prototype);
Button.prototype.constructor = Button;
module.exports = Button;

// ========================= Initialization =========================
Button.prototype.addBacking = function (atlas, image, allSettings, upSettings, overSettings, downSettings) {
	"use strict";
	this._backing = new ButtonBody(this._game, atlas, image, allSettings, upSettings, overSettings, downSettings);
	this._backing.anchor.setTo(0.5, 0.5);
	this.addChild(this._backing);
	this._buttonBodies.push(this._backing);
	this.addInputEventListeners(this._backing);
};

Button.prototype.addOutline = function (atlas, image, allSettings, upSettings, overSettings, downSettings) {
	"use strict";
	this._outline = new ButtonBody(this._game, atlas, image, allSettings, upSettings, overSettings, downSettings);
	this._outline.anchor.setTo(0.5, 0.5);
	this._outline.inputEnabled = false;
	this.addChildAt(this._outline, 0);
	this._buttonBodies.push(this._outline);
};

Button.prototype.addOverlay = function (atlas, image, allSettings, upSettings, overSettings, downSettings) {
	"use strict";
	this._overlay = new ButtonBody(this._game, atlas, image, allSettings, upSettings, overSettings, downSettings);
	this._overlay.anchor.setTo(0.5, 0.5);
	this._overlay.inputEnabled = false;
	this.addChild(this._overlay);
	this._buttonBodies.push(this._overlay);
};

Button.prototype.addIcon = function (atlas, image, settings) {
	"use strict";
	// Create icon
	this._icon = new Phaser.Image(this._game, 0, 0, atlas, image);
	this._icon.anchor.setTo(0.5, 0.5);
	this.addChild(this._icon);

	// Cache settings
	if (settings) {
		if (settings.tint) { this._icon.tint = settings.tint; }
		if (settings.align) { this._iconAlignment = settings.align; }
	}

	// Update input and view
	this.addInputEventListeners(this._icon);
};

Button.prototype.addLabel = function (text, style) {
	"use strict";
	this._label = new TextDisplay(this._game, text, style);
	this.addChild(this._label);
	this.addInputEventListeners(this._label);
};

// ========================= Settings =========================
Button.prototype.updateSettings = function (allSettings, upSettings, overSettings, downSettings) {
	"use strict";
	for (var i=0; i<this._buttonBodies; ++i) {
		this._buttonBodies[i].updateSettings(allSettings, upSettings, overSettings, downSettings);
	}
};
Button.prototype.overrideSettings = function (allSettings, upSettings, overSettings, downSettings) {
	"use strict";
	for (var i=0; i<this._buttonBodies; ++i) {
		this._buttonBodies[i].overrideSettings(allSettings, upSettings, overSettings, downSettings);
	}
};
Button.prototype.getDefaultResizeSettings = function () {
	"use strict";
	return {textWidthBounds:0.7, textHeightBounds:0.6, trueTypeFontScale:1};
};

// ========================= Properties =========================
Object.defineProperty(Button.prototype, "anchor", {
	get: function () { "use strict"; return this._touchableItem.anchor; }
});
Object.defineProperty(Button.prototype, "Backing", {
	get: function () { "use strict"; return this._backing; }
});
Object.defineProperty(Button.prototype, "Icon", {
	get: function () { "use strict"; return this._icon; }
});
Object.defineProperty(Button.prototype, "input", {
	get: function () { "use strict"; return this._touchableItem.input; }
});
Object.defineProperty(Button.prototype, "inputEnabled", {
	get: function () { "use strict"; return this._touchableItem.inputEnabled; },
	set: function (value) {
		"use strict";
		this._touchableItem.inputEnabled = value;
		if (value) { this._touchableItem.input.priorityID = this._inputPriority; }
	}
});
Object.defineProperty(Button.prototype, "Overlay", {
	get: function () { "use strict"; return this._overlay; }
});
Object.defineProperty(Button.prototype, "TextWidth", {
	get: function () { "use strict"; return this._label.TextWidth; }
});
Object.defineProperty(Button.prototype, "TextHeight", {
	get: function () { "use strict"; return this._label.TextHeight; }
});

// ========================= Input =========================
Button.prototype.addInputEventListeners = function (item) {
	"use strict";
	if (!this._touchableItem) {
		// Enable input
		this._touchableItem = item;
		this._touchableItem.inputEnabled = true;
		this._touchableItem.input.buttonMode = true;
		this._touchableItem.input.useHandCursor = true;
		this._touchableItem.input.priorityID = this._inputPriority;

		// Add events
		this._touchableItem.events.onInputUp.add(this.onInputUp, this);
		this._touchableItem.events.onInputOver.add(this.onInputOver, this);
		this._touchableItem.events.onInputOut.add(this.onInputOut, this);
		this._touchableItem.events.onInputDown.add(this.onInputDown, this);
	}
};

Button.prototype.setHitArea = function (xRatio, yRatio, widthRatio, heightRatio) {
	"use strict";
	this._customHitAreaRatios = [xRatio, yRatio, widthRatio, heightRatio];
	this._customHitArea = new PIXI.Rectangle(0,0,1,1);
	this.updateHitArea();
};

Button.prototype.updateHitArea = function () {
	"use strict";
	if (this._customHitArea) {
		this._customHitArea.x = this._touchableItem.width * this._customHitAreaRatios[0];
		this._customHitArea.y = this._touchableItem.width * this._customHitAreaRatios[1];
		this._customHitArea.width = this._touchableItem.width * this._customHitAreaRatios[2];
		this._customHitArea.height = this._touchableItem.width * this._customHitAreaRatios[3];
		this._touchableItem.hitArea = this._customHitArea;
	}
};


// ========================= Event Handlers =========================
Button.prototype.onInputUp = function () {
	"use strict";
	if (this._isInputDown && this._isInputOver) { this.OnButtonClicked.dispatch(); }
	this._isInputDown = false;
	this.updateButtonVisuals(ButtonBody.States.Up);
};
Button.prototype.onInputOver = function () {
	"use strict";
	this._isInputOver = true;
	this.updateButtonVisuals(ButtonBody.States.Over);
};
Button.prototype.onInputOut = function () {
	"use strict";
	this._isInputOver = false;
	this.updateButtonVisuals(ButtonBody.States.Up);
};
Button.prototype.onInputDown = function () {
	"use strict";
	this._isInputDown = true;
	this.updateButtonVisuals(ButtonBody.States.Down);
};

// ========================= Visuals =========================
Button.prototype.onResize = function (width, height, settings) {
	"use strict";
	// Get default settings if it doesn't exist
	if (!settings) { settings = this.getDefaultResizeSettings(); }

	// Resize the entire button
	DisplayUtils.resizeImageToBounds(this._buttonBodies[0], width, height);
	for (var i=i; i<this._buttonBodies.length; ++i) {
		this._buttonBodies[i].scale.x = this._buttonBodies[0].scale.x;
		this._buttonBodies[i].scale.y = this._buttonBodies[0].scale.y;
	}
	this.updateButtonVisuals();
	this.updateHitArea();

	// If button has both an icon and a label, align them next to each other
	if (this._icon && this._label) {
		switch (this._iconAlignment) {
			case "top":
				DisplayUtils.resizeTextDisplayToContainer(this._label, this._backing, 0.9, 0.25, settings.trueTypeFontScale);
				this._icon.y = (this._backing.height * -0.5) + (this._icon.height * 0.7);
				this._label.y = (this._backing.height * 0.5) + (this._label.TextHeight * -1.4);
				break;

			case "bottom":
				DisplayUtils.resizeTextDisplayToContainer(this._label, this._backing, 0.9, 0.25, settings.trueTypeFontScale);
				this._label.y = (this._backing.height * -0.5) + (this._label.TextHeight * 1.4);
				this._icon.y = (this._backing.height * 0.5) + (this._icon.height * -0.7);
				break;

			case "left":
				DisplayUtils.resizeTextDisplayToContainer(this._label, this._backing, 0.65, 0.6, settings.trueTypeFontScale);
				this._icon.x = (this._label.TextWidth * -0.45) + (this._icon.width * -0.5);
				this._label.x += (this._icon.width * 0.5);
				break;

			case "right":
				DisplayUtils.resizeTextDisplayToContainer(this._label, this._backing, 0.65, 0.6, settings.trueTypeFontScale);
				this._label.x = (this._label.TextWidth * -0.45) + (this._icon.width * -0.5);
				this._icon.x += (this._label.TextWidth * 0.5);
				break;
		}
	}
	//  Otherwise, just resize the text
	else {
		this.resizeText(width, height, settings); // Resize the text to make sure it fits in the button
	}
};

Button.prototype.updateButtonVisuals = function (state) {
	"use strict";
	// Update all bodies
	if (!state) { state = ButtonBody.States.Up; }
	for (var i=0; i<this._buttonBodies.length; ++i) {
		this._buttonBodies[i].updateVisuals(state);
	}
};

// ========================= Text =========================
Button.prototype.setText = function (text) {
	"use strict";
	if (this._label) { this._label.setText(text); }
};
Button.prototype.resizeText = function (width, height, settings) {
	"use strict";
	if (this._label) {
		if (!settings) { settings = this.getDefaultResizeSettings(); } // Get default settings if it doesn't exist
		if (this._backing) {
			DisplayUtils.resizeTextDisplayToContainer(this._label, this._backing,
				settings.textWidthBounds, settings.textHeightBounds, settings.trueTypeFontScale);
		}
		else { DisplayUtils.resizeTextDisplayToBounds(this._label, width, height, settings.trueTypeFontScale); }
	}
};

// ========================= Destruction =========================
Button.prototype.destroy = function () {
	"use strict";
	this.OnButtonClicked.dispose();
	if (this._touchableItem) {
		this._touchableItem.events.onInputUp.remove(this.onInputUp, this);
		this._touchableItem.events.onInputOver.remove(this.onInputOver, this);
		this._touchableItem.events.onInputOut.remove(this.onInputOut, this);
		this._touchableItem.events.onInputDown.remove(this.onInputDown, this);
	}
	Phaser.Group.prototype.destroy.call(this);
};
// ========================= Requirements =========================
var DisplayUtils = require("../utils/DisplayUtils");

// ========================= Construction =========================
var ButtonBody = function (game, atlas, image, allSettings, upSettings, overSettings, downSettings) {
	"use strict";
	// Construct
	Phaser.Sprite.call(this, game, 0, 0, atlas, image);
	this._game = game;
	this._defaultImage = image;
	this._allSettingsRef = allSettings || {};
	this._upSettingsRef = upSettings || {};
	this._overSettingsRef = overSettings || {};
	this._downSettingsRef = downSettings || {};
};

// ========================= Prototype =========================
ButtonBody.prototype = Object.create(Phaser.Sprite.prototype);
ButtonBody.prototype.constructor = ButtonBody;
module.exports = ButtonBody;

// ========================= Enums =========================
ButtonBody.States = {
	Up: 0,
	Over: 1,
	Down: 2
};
Object.freeze(ButtonBody.States);

// ========================= Initialization =========================
ButtonBody.prototype.updateSettings = function (allSettings, upSettings, overSettings, downSettings) {
	"use strict";
	if (allSettings) { this._allSettingsRef = allSettings; }
	if (upSettings) { this._upSettingsRef = upSettings; }
	if (overSettings) { this._overSettingsRef = overSettings; }
	if (downSettings) { this._downSettingsRef = downSettings; }
	this.updateVisuals();
};
ButtonBody.prototype.overrideSettings = function (allSettings, upSettings, overSettings, downSettings) {
	"use strict";
	var key;
	if (allSettings) { for (key in allSettings) { this._allSettingsRef[key] = allSettings[key]; } }
	if (upSettings) { for (key in upSettings) { this._upSettingsRef[key] = upSettings[key]; } }
	if (overSettings) { for (key in overSettings) { this._overSettingsRef[key] = overSettings[key]; } }
	if (downSettings) { for (key in downSettings) { this._downSettingsRef[key] = downSettings[key]; } }
	this.updateVisuals();
};
ButtonBody.prototype.setDefaultImage = function (frameName) {
	"use strict";
	this._defaultImage = frameName;
};

// ========================= Visuals =========================
ButtonBody.prototype.onResize = function (width, height) {
	"use strict";
	DisplayUtils.resizeImageToBounds(this, width, height);
};

ButtonBody.prototype.updateVisuals = function (state) {
	"use strict";
	// Determine which settings to use based on the state
	var settings;
	switch (state) {
		case ButtonBody.States.Up: settings = this._upSettingsRef; break;
		case ButtonBody.States.Over: settings = this._overSettingsRef; break;
		case ButtonBody.States.Down: settings = this._downSettingsRef; break;
		default: settings = this._allSettingsRef; break;
	}

	// Update generic settings
	this.updateSetting(this, "frameName", settings, "frameName", this._defaultImage);
	this.updateSetting(this, "alpha", settings, "alpha");
	this.updateSetting(this, "x", settings, "offsetX", 0);
	this.updateSetting(this, "y", settings, "offsetY", 0);
	this.updateSetting(this, "tint", settings, "tint");

	// Update specific settings
	if (settings.scale) { this.scale.x = this.scale.y = settings.scale; }
	else {
		this.updateSetting(this.scale, "x", settings, "scaleX");
		this.updateSetting(this.scale, "y", settings, "scaleY");
	}
	if (settings.visible !== undefined) {
		this.visible = settings.visible;
		if (this.visible) { this.updateTransform(); }
	}
};

ButtonBody.prototype.updateSetting = function (object, objectKey, settings, settingsKey, defaultValue) {
	"use strict";
	if (settings[settingsKey]!==undefined) { object[objectKey] = settings[settingsKey]; }
	else if (this._allSettingsRef[settingsKey]!==undefined) { object[objectKey] = this._allSettingsRef[settingsKey]; }
	else if (defaultValue!==undefined) { object[objectKey] = defaultValue; }
};

// ========================= Destruction =========================
ButtonBody.prototype.destroy = function () {
	"use strict";
	Phaser.Sprite.prototype.destroy.call(this);
};
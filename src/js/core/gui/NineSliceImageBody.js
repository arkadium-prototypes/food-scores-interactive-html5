/**
 * Created by dor on 7/1/2016.
 */
// ========================= Requirements =========================

// ========================= Construction =========================
var NineSliceImageBody = function (game, priorityId, atlas, image, settings) {
	"use strict";
	Phaser.Group.call(this, game, null);
	this._game = game;
	this._settings = settings || {};

	// If this is meant to be a tiled sprite
	if (this._settings.tileX || this._settings.tileY) {
		this._container = new Phaser.Group(this._game, this, null);
		this.addChild(this._container);
		this._tile = new Phaser.TileSprite(this._game, 0, 0, settings.tileWidth, settings.tileHeight, atlas, image);
		this._tile.anchor.setTo(0.5, 0.5);
		if (priorityId) {
			this._tile.inputEnabled = true;
			this._tile.input.priorityID = priorityId;
		}
		this._container.addChild(this._tile);
	}
	// Otherwise, it's just a plain stretchable image
	else {
		this._image = new Phaser.Image(this._game, 0, 0, atlas, image);
		this._image.anchor.setTo(0.5, 0.5);
		if (priorityId) {
			this._image.inputEnabled = true;
			this._image.input.priorityID = priorityId;
		}
		this.addChild(this._image);
	}
};

// ========================= Prototype =========================
NineSliceImageBody.prototype = Object.create(Phaser.Group.prototype);
NineSliceImageBody.prototype.constructor = NineSliceImageBody;
module.exports = NineSliceImageBody;

// ========================= Properties =========================
Object.defineProperty(NineSliceImageBody.prototype, "inputEnabled", {
	get: function () {
		"use strict";
		if (this._tile) { return this._tile.inputEnabled; }
		else if (this._image) { return this._image.inputEnabled; }
	},
	set: function (value) {
		"use strict";
		if (this._tile) { this._tile.inputEnabled = value; }
		else if (this._image) { this._image.inputEnabled = value; }
	}
});

// ========================= Resize =========================
NineSliceImageBody.prototype.onResize = function (width, height) {
	"use strict";
	// Update tiling if necessary
	if (this._tile) {
		// Tile horizontally
		if (this._settings.tileX) { // If this dimension is meant to be tiled...
			if (this._settings.tileXStep) { // Check if it needs to tile by specific steps (for pattern-tiling purposes)
				var numTiles = Math.ceil(height / this._settings.tileXStep);
				this._tile.width = numTiles * this._settings.tileXStep;
			}
			else { this._tile.width = width; } // Otherwise, there's no pattern so set to specified value
		}
		else { this._container.width = width; }

		// Tile vertically
		if (this._settings.tileY) { // If this dimension is meant to be tiled...
			if (this._settings.tileYStep) { // Check if it needs to tile by specific steps (for pattern-tiling purposes)
				var numTiles = Math.ceil(height / this._settings.tileYStep);
				this._tile.height = numTiles * this._settings.tileYStep;
			}
			else { this._tile.height = height; } // Otherwise, there's no pattern so set to specified value
		}
		else { this._container.height = height; }
	}
	// Otherwise, just stretch the image
	else {
		this._image.width = width;
		this._image.height = height;
	}

	// Update the rest of the settings
	var image = this._tile || this._image;
	if (this._settings.frameName) { image.frameName = this._settings.frameName; }
	if (this._settings.alpha) { image.alpha = this._settings.alpha; }
	if (this._settings.offsetX) { this.x = this._settings.offsetX; }
	else { this.x = 0; }
	if (this._settings.offsetY) { this.y = this._settings.offsetY; }
	else { this.y = 0; }
	if (this._settings.scale) { image.scale.x = this._image.scale.y = this._settings.scale; }
	else {
		if (this._settings.scaleX) { image.scale.x = this._settings.scaleX; }
		if (this._settings.scaleY) { image.scale.y = this._settings.scaleY; }
	}
	if (this._settings.tint) { image.tint = this._settings.tint; }
	if (this._settings.uniformX) {
		image.scale.x = image.scale.y;
		if (this._settings.scaleX) { image.scale.x *= this._settings.scaleX; }
	}
	else if (this._settings.uniformY) {
		image.scale.y = image.scale.x;
		if (this._settings.scaleY) { image.scale.y *= this._settings.scaleY; }
	}
	if (this._settings.visible !== undefined) {
		this.visible = this._settings.visible;
		if (this.visible) { this._image.updateTransform(); }
	}
};

// ========================= Destruction =========================
NineSliceImageBody.prototype.destroy = function () {
	"use strict";
	if (this._image) { this._image.destroy(true); }
	if (this._tile) { this._tile.destroy(true); }
	if (this._container) { this._container.destroy(true); }
	Phaser.Image.prototype.destroy.call(this);
};
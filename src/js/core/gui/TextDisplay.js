// ========================= Requirements =========================
var DisplayUtils = require("../utils/DisplayUtils");

// ========================= Construction =========================
/**
 * A generic text display that will switch between bitmap and vector text based on the game's settings
 * @param game
 * @param text
 * @param style
 * @constructor
 */
var TextDisplay = function (game, text, style) {
	"use strict";
	Phaser.Group.call(this, game, null);

	// Set references
	this._game = game;
	this._styleRef = style;
	this._numLines = 0;
	this._text = text;

	// Initialize variables
	this._allowBitmapText = true;
	this._isBitmapText = false;
	this._wrappedTextPages = null;

	// Define some default values in case it's not set
	if (!this._styleRef.fontResolution) { this._styleRef.fontResolution = 1; }
	if (!this._styleRef.lineSpacing) { this._styleRef.lineSpacing = 0; }

	// Use BitmapText for better efficiency
	if (this._allowBitmapText && (this._styleRef.bitmapText === true)) {
		this._isBitmapText = true;
		this._textField = new Phaser.BitmapText(this._game, 0, 0, this._styleRef.font, text, this._styleRef.fontSize);
		this._textField.align = this._styleRef.align || "center";
		this._textField.inputEnabled = false;
		this._textField.tint = this._styleRef.fill;
		this.addChild(this._textField);
		this.wrapText();
	}
	// Or use regular Text if word wrapping is required
	else {
		this._isBitmapText = false;
		this._styleRef.font = (this._styleRef.fontSize * this._styleRef.fontResolution) + "px " + this._styleRef.font;
		this._styleRef.fill = "#" + DisplayUtils.intToRGB(this._styleRef.fill);
		this._textField = new Phaser.Text(this._game, 0, 0, this._text, this._styleRef);
		this._textField.align = this._styleRef.align || "center";
		this._textField.inputEnabled = false;
		this._textField.lineSpacing = this._styleRef.lineSpacing;
		this._textField.scale.x = this._textField.scale.y = 1 / this._styleRef.fontResolution;
		this.addChild(this._textField);
		this.wrapText();
	}
};

// ========================= Prototype =========================
TextDisplay.prototype = Object.create(Phaser.Group.prototype);
TextDisplay.prototype.constructor = TextDisplay;
module.exports = TextDisplay;

// ========================= Methods =========================
TextDisplay.prototype.updateTransform = function () {
	"use strict";
	Phaser.Group.prototype.updateTransform.call(this);
	this._textField.updateTransform();
};
TextDisplay.prototype.setText = function (text) {
	"use strict";
	this._text = text;
	this._textField.setText(text);
	this.updateTransform();
};
TextDisplay.prototype.setTextToOriginal = function () {
	"use strict";
	this._textField.setText(this._text);
};
TextDisplay.prototype.wrapText = function (width, height) {
	"use strict";
	if (width!=null) { this._styleRef.width = width; } // Override the width of the textbox
	if (height!=null) { this._styleRef.height = height; } // Override the height of the textbox
	if (!this._styleRef.width || !this._styleRef.height) { return; } // Don't wrap text if there's no dimensions set

	// If this is bitmap text, then use the TextWrapper class
	if (this._isBitmapText) {
		this._wrappedTextPages = Helper.TextWrapper.wrapText(this._game, this._text, this._styleRef.width, this._styleRef.height, this._styleRef.font, this._styleRef.fontSize);
		this._textField.setText(this._wrappedTextPages[0]);
	}
	// Otherwise, if this is vector text, use built-in word wrapping parameters
	else {
		this._textField.wordWrap = true;
		this._textField.wordWrapWidth = this._styleRef.width * this._styleRef.fontResolution;
	}
	this.updateTransform();
};


// ========================= Colors =========================
TextDisplay.prototype.addColor = function (color, startPos, endPos) {
	"use strict";
	if (this._isBitmapText) {
		for (var i=startPos, len=Math.min(endPos, this._textField.children.length); i<len; ++i) {
			this._textField.children[i].tint = color;
		}
	}
	else {
		this._textField.addColor("#" + DisplayUtils.intToRGB(color), startPos);
		this._textField.addColor("#ffffff", endPos);
	}
};

TextDisplay.prototype.clearColors = function () {
	"use strict";
	if (this._isBitmapText) {
		for (var i=0, len=this._textField.children.length; i<len; ++i) {
			this._textField.children[i].tint = "0xFFFFFF";
		}
	}
	else {
		this._textField.clearColors();
	}
};

// ========================= Properties =========================
Object.defineProperty(TextDisplay.prototype, "events", {
	get: function () { "use strict"; return this._textField.events; }
});
Object.defineProperty(TextDisplay.prototype, "fontSize", {
	get: function() { "use strict";
		if (this._isBitmapText) { return this._textField.fontSize; }
		else { return this._textField.fontSize / this._styleRef.fontResolution; }
	},
	set: function(value) {
		"use strict";
		this._styleRef.fontSize = value;
		if (this._isBitmapText) { this._textField.fontSize = value; }
		else {
			this._textField.fontSize = (value * this._styleRef.fontResolution);
			this._textField.scale.x = this._textField.scale.y = 1 / this._styleRef.fontResolution;
		}
		this.updateTransform();
	}
});
Object.defineProperty(TextDisplay.prototype, "inputEnabled", {
	get: function () { "use strict"; return this._textField.inputEnabled; },
	set: function (value) { "use strict"; this._textField.inputEnabled = value; }
});
Object.defineProperty(TextDisplay.prototype, "input", {
	get: function () { "use strict"; return this._textField.input; }
});
Object.defineProperty(TextDisplay.prototype, "Fill", {
	get: function() {
		"use strict";
		if (this._isBitmapText) { return this._textField.tint; }
		else { return this._styleRef.fill; }
	},
	set: function(value) {
		"use strict";
		if (this._isBitmapText) {
			this._styleRef.fill = value;
			this._textField.tint = value;
		}
		else {
			this._styleRef.fill = value;
			this._textField.fill = "#" + DisplayUtils.intToRGB(value);
		}
	}
});
Object.defineProperty(TextDisplay.prototype, "IsBitmapText", {
	get: function() { "use strict"; return this._isBitmapText; }
});
Object.defineProperty(TextDisplay.prototype, "NumLines", {
	get: function() { "use strict"; return this._numLines; }
});
Object.defineProperty(TextDisplay.prototype, "Text", {
	get: function() { "use strict"; return this._text; }
});
Object.defineProperty(TextDisplay.prototype, "TextField", {
	get: function() { "use strict"; return this._textField; }
});
Object.defineProperty(TextDisplay.prototype, "TextWidth", {
	get: function() { "use strict"; return this._textField.textWidth || this._textField.width; }
});
Object.defineProperty(TextDisplay.prototype, "TextHeight", {
	get: function() { "use strict"; return this._textField.textHeight || this._textField.height; }
});
Object.defineProperty(TextDisplay.prototype, "WrappedTextPages", {
	get: function() { "use strict"; return this._wrappedTextPages; }
});


// ========================= Destruction =========================
TextDisplay.prototype.destroy = function () {
	"use strict";
	Phaser.Group.prototype.destroy.call(this);
};
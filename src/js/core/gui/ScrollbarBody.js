/**
 * Created by dor on 6/29/2016.
 */
// ================ Requirements ================

// ================ Construction ================
var ScrollbarBody = function (game, isVertical, settings) {
	"use strict";
	Phaser.Group.call(this, game, null);
	this._isVertical = isVertical;
	this._game = game;
	this._defaultTint = settings.tint || 0xFFFFFF;
	this._downTint = settings.downTint || 0x555555;

	// Add the scrollbar image
	this._imageMid = new Phaser.Group(this._game, this, null);
	this.addChild(this._imageMid);
	this._imageMidTile = new Phaser.TileSprite(this._game, 0, 0, 27 * settings.assetScale, 1, settings.atlas, settings.imageMid);
	this._imageMidTile.anchor.setTo(0.5, 0.5);
	this._imageMidTile.tint = this._defaultTint;
	if (!this._isVertical) { this._imageMid.angle = 90; }
	this._imageMid.addChild(this._imageMidTile);

	// Add caps to the scrollbar
	this._imageCaps = [];
	if (settings.imageCap !== undefined) {
		for (var i=0; i<2; ++i) {
			var newCap = new Phaser.Image(this._game, 0, 0, settings.atlas, settings.imageCap);
			newCap.anchor.setTo(0.5, 0.5);
			newCap.tint = this._defaultTint;
			if (!this._isVertical) { newCap.angle = 90; }
			if (i===1) { newCap.scale.x = newCap.scale.y = -1; } // Flip the second cap
			this.addChild(newCap);
			this._imageCaps.push(newCap);
		}
	}

	// Add an icon to the scrollbar
	if (settings.imageIcon !== undefined) {
		this._imageIcon = new Phaser.Image(this._game, 0, 0, settings.atlas, settings.imageIcon);
		this._imageIcon.anchor.setTo(0.5, 0.5);
		this._imageIcon.tint = this._defaultTint;
		if (!this._isVertical) { this._imageIcon.angle = 90; }
		this.addChild(this._imageIcon);
	}

	// Initialize
	this.buttonMode = true;
	this.useHandCursor = true;
	this.updateVisualStatus(false);
};

// ================ Prototype ================
ScrollbarBody.prototype = Object.create(Phaser.Group.prototype);
ScrollbarBody.prototype.constructor = ScrollbarBody;
module.exports = ScrollbarBody;

// ================ Update ================
ScrollbarBody.prototype.updateVisualStatus = function (isBeingDragged) {
	"use strict";
	this._imageMidTile.tint = (isBeingDragged) ? (this._downTint) : (this._defaultTint);
	for (var i=0; i<this._imageCaps.length; ++i) {
		this._imageCaps[i].tint = (isBeingDragged) ? (this._downTint) : (this._defaultTint);
	}
};

ScrollbarBody.prototype.updatePositionAndSize = function (x, y, width, height) {
	"use strict";
	this.x = x;
	this.y = y;

	// If the scrollbar body has caps
	if (this._imageCaps.length > 0) {
		if (this._isVertical) {
			// Body
			this._imageMid.width = width;
			this._imageMidTile.height = height - this._imageCaps[0].height;

			// Top cap
			this._imageCaps[0].y = (height * 0.5) - (this._imageCaps[0].height * 0.5);
			this._imageCaps[0].width = width;
			this._imageCaps[0].height = width * -0.5;

			// Bottom cap
			this._imageCaps[1].y = (height * -0.5) + (this._imageCaps[0].height * 0.5);
			this._imageCaps[1].width = width;
			this._imageCaps[1].height = width * 0.5;

			// Icon
			if (this._imageIcon) {
				this._imageIcon.width = width * 0.73;
				this._imageIcon.height = width * 0.86;
			}
		}
		else {
			// Body
			this._imageMid.width = height;
			this._imageMidTile.height = width - this._imageCaps[0].height;

			// Left cap
			this._imageCaps[0].x = (width * -0.5) + this._imageCaps[0].height;
			this._imageCaps[0].width = height;
			this._imageCaps[0].height = height * -0.5;

			// Right cap
			this._imageCaps[1].x = (width * 0.5) - this._imageCaps[0].height;
			this._imageCaps[1].width = height;
			this._imageCaps[1].height = height * 0.5;

			// Icon
			if (this._imageIcon) {
				this._imageIcon.width = height * 0.86;
				this._imageIcon.height = height * 0.73;
			}
		}
	}
	// Otherwise, the scrollbar body doesn't have caps
	else {
		this._imageMid.width = width;
		this._imageMid.height = height;
	}
};
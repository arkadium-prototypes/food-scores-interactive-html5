/**
 * Graphical User Interface Layout Element
 * A class that contains multiple display elements, but is not a display object itself
 * This way, elements can exist on different display layers, for drawcall optimization
 * But all positioning, scaling, etc. is handled within this class
 */

// =============== Construction ===============
var Guile = function(game) {
    "use strict";
    this._game = game;
    this._elements = [];
    this._transformMatrix = new PIXI.Matrix();
};

// =============== Prototype ===============
Guile.prototype = {
    addChildTo: function(child, layer) {
        layer.addChild(child);
        this._elements.push(child);
    },
    sonicBoom: function() {
        console.log("Sonic BOOM!");
    },
    flashKick: function() {
        console.log("Flash Kick!");
    }
};

// =============== Registration ===============
Guile.prototype = Object.create(Guile.prototype);
Guile.prototype.constructor = Guile;
module.exports = Guile;

// =============== Properties ===============
Object.defineProperty(Guile.prototype, "x", {
    get: function () {
        "use strict";
        return this._transformMatrix.tx;
    },
    set: function(value) {
        "use strict";
        this._transformMatrix.tx = value;
        for (var i=0, len=this._elements.length; i<len; ++i) {
            var wt = this._transformMatrix.append(this._elements[i].transform);
            this._elements[i].x = wt.tx;
        }
    }
});

Object.defineProperty(Guile.prototype, "y", {
    get: function () {
        "use strict";
        return this._transformMatrix.ty;
    },
    set: function(value) {
        "use strict";
        this._transformMatrix.ty = value;
        for (var i=0, len=this._elements.length; i<len; ++i) {
            var wt = this._transformMatrix.append(this._elements[i].worldTransform);
            this._elements[i].y = wt.ty;
        }
    }
});
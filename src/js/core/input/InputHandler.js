/**
 * Interface for registering to input events. Catches Phaser input events and distributes them to objects that care to
 * know about them
 * @constructor
 */
var InputHandler = function () {
    "use strict";
    this.game = null;
    this.OnKeyDown = new Phaser.Signal();
    this.OnKeyUp = new Phaser.Signal();
    this.OnKeyPress = new Phaser.Signal();

    this.OnPointerDown = new Phaser.Signal();
    this.OnPointerUp = new Phaser.Signal();
    this.OnPointerMove = new Phaser.Signal();

	this.OnMouseWheelMove = new Phaser.Signal();
    this.moveCallbackIndex = -1;
};
module.exports = InputHandler;
InputHandler.instance = new InputHandler();

/**
 * Initializes callbacks to Phaser events, so that this class's signals know when to dispatch
 * @param game  Reference to phaser game
 */
InputHandler.prototype.initialize = function (game, allowMouseWheel) {
    "use strict";
    this.game = game;
    this.removeEventListeners();
	this.game.input.keyboard.addCallbacks(this, this.onPhaserKeyDown, this.onPhaserKeyUp, this.onPhaserKeyPress);
    this.game.input.onDown.add(this.onPhaserPointerDown, this);
    this.game.input.onUp.add(this.onPhaserPointerUp, this);
    this.moveCallbackIndex = this.game.input.addMoveCallback(this.onPhaserPointerMove, this);
	if (allowMouseWheel) { this.game.input.mouse.mouseWheelCallback = this.onPhaserMouseWheel.bind(this); }
};

/**
 * Dispatches an event when a key is down
 * @param event     Keyboard event data
 */
InputHandler.prototype.onPhaserKeyDown = function (event) {
    "use strict";
    // #fix: If no modifier keys are held down, and the key is not F12, then prevent default behavior
    // This override Firefox's "Search for text when I start typing" option
    if (!event.shiftKey && !event.ctrlKey && !event.altKey && event.keyCode!==Phaser.Keyboard.F12) {
        event.preventDefault();
    }

    this.OnKeyDown.dispatch(event);
};

/**
 * Dispatches an event when a key is up
 * @param event     Keyboard event data
 */
InputHandler.prototype.onPhaserKeyUp = function (event) {
    "use strict";
    this.OnKeyUp.dispatch(event);
};

/**
 * Dispatches an event when a key is pressed
 * @param event     Keyboard event data
 */
InputHandler.prototype.onPhaserKeyPress = function (event) {
    "use strict";
    this.OnKeyPress.dispatch(event);
};

/**
 * Dispatches an event when a pointer is down
 * @param event     Pointer event data
 */
InputHandler.prototype.onPhaserPointerDown = function (event) {
    "use strict";
    this.OnPointerDown.dispatch(event);
};

/**
 * Dispatches an event when a pointer is up
 * @param event     Pointer event data
 */
InputHandler.prototype.onPhaserPointerUp = function (event) {
    "use strict";
    this.OnPointerUp.dispatch(event);
};

/**
 * Dispatches an event when a pointer is moved
 * @param pointer   The pointer that was moved
 * @param xPos      X position of the pointer
 * @param yPos      Y position of the pointer
 * @param downState Whether the pointer is down
 */
InputHandler.prototype.onPhaserPointerMove = function (pointer, xPos, yPos, downState) {
    "use strict";
    this.OnPointerMove.dispatch(pointer, xPos, yPos, downState);
};

/**
 * Dispatches an event when the mouse wheel is moved up or down
 */
InputHandler.prototype.onPhaserMouseWheel = function () {
	"use strict";
	this.OnMouseWheelMove.dispatch();
};

/**
 * Removes phaser input event callbacks
 */
InputHandler.prototype.removeEventListeners = function () {
    "use strict";
    this.game.input.deleteMoveCallback(this.moveCallbackIndex);
    this.game.input.onUp.remove(this.onPhaserPointerUp, this);
    this.game.input.onDown.remove(this.onPhaserPointerDown, this);
    this.game.input.keyboard.addCallbacks(null, null, null, null);
	this.game.input.mouse.mouseWheelCallback = null;
};

/**
 * Removes phaser input event callbacks
 */
InputHandler.prototype.destroy = function () {
    "use strict";
    this.removeEventListeners();
};
/**
 * Phaser plugin that controls when the game should render based on fps, tweens, or custom events
 * @param game      Reference to phaser game
* @param parent        Phaser's plugin manager
 * @constructor
 */
var RenderHandler = function (game, parent) {
    "use strict";
    Phaser.Plugin.call(this, game, parent);
    RenderHandler.instance = this;

    this.now = window.performance.now();
    this.renderDirty = false;

    this.renderOnFPS = 30;
    this.renderOnPointerChange = false;
    this.renderDuringTweens = false;
};
module.exports = RenderHandler;
RenderHandler.prototype = Object.create(Phaser.Plugin.prototype);
RenderHandler.prototype.constructor = RenderHandler;

/**
 * Adds a callback that forces the game to render when the passed-in signal dispatches an event
 * @param signal    The signal that should force a redraw when dispatched
 */
RenderHandler.prototype.addEventHandler = function (signal) {
    "use strict";
    signal.add(this.forceRender, this);
};

/**
 * Adds a callback that forces the game to render when the passed-in signal dispatches an event
 * @param signal    The signal that should force a redraw when dispatched
 */
RenderHandler.prototype.removeEventHandler = function (signal) {
    "use strict";
    signal.remove(this.forceRender, this);
};

/**
 * Locks or unlocks phaser's rendering function depending on whether a dirty flag was set or not
 */
RenderHandler.prototype.setRender = function () {
    'use strict';
    if (this.renderDirty) {
        this.game.lockRender = false;
    } else {
        this.game.lockRender = true;
    }
    this.renderDirty = false;
};

/**
 * Sets a dirty flag, forcing the game to render for the current frame
 */
RenderHandler.prototype.forceRender = function () {
    "use strict";
    this.renderDirty = true;
};

/**
 * Forces the game to render if the current frame lines up with the rendering frame rate we have set
 * @returns {boolean}   Whether the game should render
 */
RenderHandler.prototype.forceRenderOnFPS = function () {
    "use strict";
    var ts, diff;

    ts = window.performance.now();
    diff = ts - this.now;
    if (diff < (1000 / this.renderOnFPS)) {
        return false;
    }
    this.now = ts;
    this.forceRender();
    return true;
};

/**
 * Forces teh game to render if the pointer is moving while pressed down, or the the pointer clicked/tapped
 */
RenderHandler.prototype.forceRenderOnPointerChange = function () {
    "use strict";
    var input = this.game.input;

    if (input.activePointer.isDown && (input.oldx !== input.x || input.oldy !== input.y)) {
        this.forceRender();
        input.oldx = input.x;
        input.oldy = input.y;
    }
    if (input.oldDown !== input.activePointer.isDown) {
        this.forceRender();
        input.oldDown = input.activePointer.isDown;
    }
};

// ========================= Phaser plugin functions =========================
/**
 * Forces a render if there are any active tweens
 */
RenderHandler.prototype.preUpdate = function () {
    "use strict";
    if (this.renderDuringTweens && this.game.tweens.getAll().length > 0) {
        this.forceRender();
    }
};

/**
 * Checks if we should force a render depending on settings
 */
RenderHandler.prototype.postUpdate = function () {
    "use strict";
    if (this.renderOnFPS && this.forceRenderOnFPS()) {
        this.setRender();
        return;
    }
    if (this.renderOnPointerChange && this.forceRenderOnPointerChange()) {
        this.setRender();
        return;
    }
    this.setRender();
};

/**
 * Defaults rendering to locked each frame
 */
RenderHandler.prototype.postRender = function () {
    "use strict";
    if (this.game._paused) {
        this.game.lockRender = true;
    }
};
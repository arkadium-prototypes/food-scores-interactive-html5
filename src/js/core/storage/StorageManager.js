// ========================= Construction =========================
/**
 * Manages loading and saving data to local storage
 * @constructor
 */
var StorageManager = function () {
    "use strict";
    this._allowLocalStorage = true;
    this._gameID = "";
};
module.exports = StorageManager;

// ========================= Properties =========================
Object.defineProperty(StorageManager.prototype, "AllowLocalStorage", {
    get: function () { "use strict"; return this._allowLocalStorage; },
    set: function (value) { "use strict"; this._allowLocalStorage = value; }
});

// ========================= Initialization =========================
StorageManager.prototype.initialize = function(gameID) {
    "use strict";
    this._gameID = gameID;
};

// ========================= Loading =========================
StorageManager.prototype.loadData = function(key) {
    "use strict";
    key = this._gameID + key;
    if (this._allowLocalStorage) {
        try {
            var data = localStorage.getItem(key);
            if (data) { return data; }
        }
        catch(error) {
			console.log("StorageManager.loadData("+key+")", error.message);
            this._allowLocalStorage = false;
        }
    }
	return null;
};

// ========================= Saving =========================
StorageManager.prototype.saveData = function(key, value) {
    "use strict";
    key = this._gameID + key;
    if (this._allowLocalStorage) {
        try {
            localStorage.setItem(key, value);
            return true; // Progress was successfully saved!
        }
        catch(error) {
            console.log("StorageManager.saveData("+key+")", error.message);
            this._allowLocalStorage = false;
            return false; // Saving failed!
        }
    }
};
/**
 * Created by davidor on 5/1/2015.
 */

// ========================= Construction =========================
/**
 * Manages loading and showing advertisements
 * @constructor
 */
var AdManager = function () {
    "use strict";
    this.allowInterstitialAds = false;
    this.interstitialAd = null;
    this.interstitialAdState = 0;
    this.OnAdShownEvent = new Phaser.Signal();
};
module.exports = AdManager;
AdManager.instance = new AdManager();

// ========================= Constants =========================
AdManager.AdStates = {
    unloaded: 0,
    loading: 1,
    loaded: 2
};
Object.freeze(AdManager.AdStates);

// ========================= Properties =========================
Object.defineProperty(AdManager.prototype, "IsInterstitialAdLoaded", {
    get: function () {
        "use strict";
        return this.interstitialAdState === AdManager.AdStates.loaded;
    }
});

// ========================= Initialization =========================
/**
 * Sets puzzle loading values, either from arena or using defualts
 */
AdManager.prototype.initialize = function(allowInterstitialAds, adID) {
    "use strict";
    console.log("AdManager.init");
    this.allowInterstitialAds = allowInterstitialAds;
    if (this.allowInterstitialAds) {
        console.log("allowInterstitialAds");
        // Configure ads and create the interstitial
        Cocoon.Ad.Heyzap.configure({publisherId:adID});
        this.interstitialAd = Cocoon.Ad.Heyzap.createInterstitial();

        // If interstitial exists, then add event listeners
        if (this.interstitialAd != null) {
            console.log("this.interstitialAd != null");

            this.interstitialAd.on("load", (function () {
                console.log("AdManager.interstitialAd.on(load)");
                this.interstitialAdState = AdManager.AdStates.loaded;
            }).bind(this));

            this.interstitialAd.on("show", (function () {
                console.log("AdManager.interstitialAd.on(show)");
                this.OnAdShownEvent.dispatch();
            }).bind(this));

            this.interstitialAd.on("fail", (function () {
                console.log("AdManager.interstitialAd.on(fail)");
                this.interstitialAdState = AdManager.AdStates.unloaded;
            }).bind(this));

            this.interstitialAd.on("dismiss", (function () {
                console.log("AdManager.interstitialAd.on(dismiss)");
            }).bind(this));

            this.interstitialAd.on("click", (function () {
                console.log("AdManager.interstitialAd.on(click)");
            }).bind(this));
        }

        this.loadInterstitial();
    }
};

// ========================= Interstitial ad =========================
AdManager.prototype.loadInterstitial = function() {
    "use strict";
    if (this.allowInterstitialAds && this.interstitialAd != null &&
        this.interstitialAdState !== AdManager.AdStates.loaded) {
        console.log("AdManager.loadInterstitial()");
        this.interstitialAdState = AdManager.AdStates.loading;
        this.interstitialAd.load();
    }
};

AdManager.prototype.refreshInterstitial = function() {
    "use strict";
    if (this.allowInterstitialAds && this.interstitialAd != null) {
        console.log("AdManager.refreshInterstitial()");
        this.interstitialAdState = AdManager.AdStates.loading;
        this.interstitialAd.load();
    }
};

AdManager.prototype.showInterstitial = function() {
    "use strict";
    if (this.allowInterstitialAds && this.interstitialAd != null) {
        console.log("AdManager.showInterstitial()");
        this.interstitialAdState = AdManager.AdStates.unloaded;
        this.interstitialAd.show();
    }
};
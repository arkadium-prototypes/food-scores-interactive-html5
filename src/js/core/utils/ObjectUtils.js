var ObjectUtils = {
    containsObject: function(obj, list) {
        "use strict";
        var i;
        for (i = 0; i < list.length; i++) {
            if (list[i] === obj) {
                return true;
            }
        }
        return false;
    },

    cloneObject: function(obj) {
        "use strict";
        if (obj === null || typeof(obj) !== "object") {
            return obj;
        }

        var temp = obj.constructor(); // changed
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                temp[key] = this.cloneObject(obj[key]);
            }
        }
        return temp;
    },

    doesFileExist: function(urlToFile) {
        "use strict";
        var xhr = new XMLHttpRequest();
        xhr.open("HEAD", urlToFile, false);

        try {
            xhr.send();
            var xhrStatus = xhr.status.toString();
            // Accepts all 2xx and 3xx server responses
            if ((xhrStatus.charAt(0) == "2") || (xhrStatus.charAt(0) == "3")) {
                return true;
            } else {
                return false;
            }
        }
        catch (e) {
            console.log("utils.doesFileExist()", urlToFile, e.name, e.message);
            return false;
        }
    }
};
module.exports = ObjectUtils;
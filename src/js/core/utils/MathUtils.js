var MathUtils = {
    constrain: function(min, value, max) {
        "use strict";
        return Math.max(min, Math.min(value, max));
    },

    countNumDigits: function(n) {
        "use strict";
        var numDigits = 0;
        while (n>=1) {
            n /= 10;
            ++numDigits;
        }
        return numDigits;
    }
};
module.exports = MathUtils;
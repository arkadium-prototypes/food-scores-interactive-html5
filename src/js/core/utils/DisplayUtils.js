var DisplayUtils = {
    intToRGB: function(i) {
        "use strict";
        var r = ((i>>16)&0xFF).toString(16);
        if (r.length < 2) { r = "0" + r; }
        var g = ((i>>8)&0xFF).toString(16);
        if (g.length < 2) { g = "0" + g; }
        var b = (i&0xFF).toString(16);
        if (b.length < 2) { b = "0" + b; }
        return r + g + b;
    },

    isImageWithinBounds: function (image, bounds) {
        "use strict";
        return (image.width <= bounds) && (image.height <= bounds);
    },

    resizeImageToBounds: function(image, widthBounds, heightBounds) {
        "use strict";
        if (widthBounds && heightBounds) {
            var newWidth = heightBounds * image.width / image.height;
            if (newWidth > widthBounds) {
                image.width = widthBounds;
                image.scale.y = image.scale.x;
            }
            else {
                image.height = heightBounds;
                image.scale.x = image.scale.y;
            }
        }
        else if (widthBounds) {
            image.width = widthBounds;
            image.scale.y = image.scale.x;
        }
        else if (heightBounds) {
            image.height = heightBounds;
            image.scale.x = image.scale.y;
        }
    },

    resizeImageToContainerRatios: function(image, container, widthRatio, heightRatio, xRatio, yRatio) {
        "use strict";
        DisplayUtils.resizeImageToBounds(image, container.width * widthRatio, container.height * heightRatio);
        image.x = container.width * xRatio;
        image.y = container.height * yRatio;
    },

    resizeTextDisplayToBounds: function (textDisplay, widthBound, heightBound, trueTypeFontScale) {
        "use strict";
        textDisplay.fontSize = heightBound;
        if (textDisplay.TextWidth > widthBound) {
            var targetHeight = textDisplay.TextHeight * widthBound / textDisplay.TextWidth;
            if (!trueTypeFontScale) { trueTypeFontScale = 1; }
            if (!textDisplay.IsBitmapText) { targetHeight *= trueTypeFontScale; }
            textDisplay.fontSize = targetHeight;
        }
    },

    resizeTextDisplayToContainer: function (textDisplay, container, maxWidthRatio, maxHeightRatio, trueTypeFontScale) {
        "use strict";
        textDisplay.fontSize = container.height * maxHeightRatio;
        if (textDisplay.TextWidth > container.width * maxWidthRatio) {
            var targetHeight = textDisplay.TextHeight * (container.width * maxWidthRatio) / textDisplay.TextWidth;
            if (!trueTypeFontScale) { trueTypeFontScale = 1; }
            if (!textDisplay.IsBitmapText) { targetHeight *= trueTypeFontScale; }
            textDisplay.fontSize = targetHeight;
        }
        textDisplay.x = (container.width * (0.5 - container.anchor.x)) - (textDisplay.TextWidth * 0.5);
        textDisplay.y = (container.height * (0.5 - container.anchor.y)) - (textDisplay.TextHeight * 0.5);
    },

    setHeightBounds: function (image, minHeight, maxHeight) {
        "use strict";
        if (minHeight != null) {
            if (image.width / image.height > minHeight){
                image.height = image.width / minHeight;
            }
        }
        if (maxHeight != null) {
            if (image.width / image.height < maxHeight){
                image.height = image.width / maxHeight;
            }
        }
    },

    setWidthBounds: function (image, minWidth, maxWidth) {
        "use strict";
        if (minWidth != null) {
            if (image.width / image.height < minWidth){
                image.width = image.height * minWidth;
            }
        }
        if (maxWidth != null) {
            if (image.width / image.height > maxWidth){
                image.width = image.height * maxWidth;
            }
        }
    }
};
module.exports = DisplayUtils;
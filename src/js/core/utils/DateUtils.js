var DateUtils = {
	/**
     * Returns the localized string for a given day (e.g. Sunday, Monday, Tuesday...)
     * @param day {int} 0=Sunday, 1=Monday, 2=Tuesday, 3=Wednesday, 4=Thursday, 5=Friday, 6=Saturday
     * @param showFullNames {Boolean} If true, shows full name (e.g. Sunday). If false, shows shortened name (e.g. Sun)
     * @returns {string} Localized string for the given day
     */
    getDayOfWeek: function(day, showFullNames) {
        "use strict";
        switch (day) {
            case 0: return (showFullNames) ? LocalizationManager.getText("date.day.full.sunday"): LocalizationManager.getText("date.day.short.sunday");
            case 1: return (showFullNames) ? LocalizationManager.getText("date.day.full.monday"): LocalizationManager.getText("date.day.short.monday");
            case 2: return (showFullNames) ? LocalizationManager.getText("date.day.full.tuesday"): LocalizationManager.getText("date.day.short.tuesday");
            case 3: return (showFullNames) ? LocalizationManager.getText("date.day.full.wednesday"): LocalizationManager.getText("date.day.short.wednesday");
            case 4: return (showFullNames) ? LocalizationManager.getText("date.day.full.thursday"): LocalizationManager.getText("date.day.short.thursday");
            case 5: return (showFullNames) ? LocalizationManager.getText("date.day.full.friday"): LocalizationManager.getText("date.day.short.friday");
            case 6: return (showFullNames) ? LocalizationManager.getText("date.day.full.saturday"): LocalizationManager.getText("date.day.short.saturday");
        }
    },

	/**
	 * Returns the localized string for a given month (e.g. January, February, March...)
	 * @param month {int} 0=January, 1=February, 2=March, 3=April, 4=May, 5=June, 6=July, 7=August, 8=Sept, 9=Oct, 10=Nov, 11=Dec
	 * @param showFullNames {Boolean} If true, shows full name (e.g. January). If false, shows shortened name (e.g. Jan)
	 * @returns {string} Localized string for the given month
	 */
    getMonthName: function(month, showFullNames) {
        "use strict";
        switch (month) {
            case 0: return (showFullNames) ? LocalizationManager.getText("date.month.full.january"): LocalizationManager.getText("date.month.short.january");
            case 1: return (showFullNames) ? LocalizationManager.getText("date.month.full.february"): LocalizationManager.getText("date.month.short.february");
            case 2: return (showFullNames) ? LocalizationManager.getText("date.month.full.march"): LocalizationManager.getText("date.month.short.march");
            case 3: return (showFullNames) ? LocalizationManager.getText("date.month.full.april"): LocalizationManager.getText("date.month.short.april");
            case 4: return (showFullNames) ? LocalizationManager.getText("date.month.full.may"): LocalizationManager.getText("date.month.short.may");
            case 5: return (showFullNames) ? LocalizationManager.getText("date.month.full.june"): LocalizationManager.getText("date.month.short.june");
            case 6: return (showFullNames) ? LocalizationManager.getText("date.month.full.july"): LocalizationManager.getText("date.month.short.july");
            case 7: return (showFullNames) ? LocalizationManager.getText("date.month.full.august"): LocalizationManager.getText("date.month.short.august");
            case 8: return (showFullNames) ? LocalizationManager.getText("date.month.full.september"): LocalizationManager.getText("date.month.short.september");
            case 9: return (showFullNames) ? LocalizationManager.getText("date.month.full.october"): LocalizationManager.getText("date.month.short.october");
            case 10: return (showFullNames) ? LocalizationManager.getText("date.month.full.november"): LocalizationManager.getText("date.month.short.november");
            case 11: return (showFullNames) ? LocalizationManager.getText("date.month.full.december"): LocalizationManager.getText("date.month.short.december");
        }
    },

	/**
	 * For English only, returns the suffix for a given number (e.g. 1st, 2nd, 3rd, 4th...)
	 * @param number {int} The number to add the suffix to
	 * @returns {string} The suffix to be added to the given number
	 */
    getOrdinalSuffix: function(number) {
        "use strict";
        // All numbers between 10-20 use "th"
        // Example: 11th, 12th, 13th, 14th...
        if (number % 100 > 10 && number % 100 < 20) {
            return "th";
        }

        // In all other cases, we look at the last-digit to determine the suffix
        // Example: 1st, 2nd, 3rd, 4th, 5th, 6th..
        switch (number % 10){
            case 1: return "st";
            case 2: return "nd";
            case 3: return "rd";
            default: return "th";
        }
    },

	/**
	 * Converts milliseconds to a Minutes:Second time format
	 * @param ms {int} Number of milliseconds in the time
	 * @returns {string}
	 */
    convertMStoTime: function(ms) {
        "use strict";
        var mins = Math.floor(ms / 60000);
        var secs = Math.floor(ms / 1000) % 60;
        var secString = (secs > 9) ? ("" + secs) : ("0" + secs);
        return mins + ":" + secString;
    },

	/**
	 * Strips a numerical date from a string
	 * @param {string}
	 * @returns {string} A string without the numerical date
	 */
    stripDateFromString: function(string) {
        "use strict";
        string.replace(/(\d)?[- /.](\d)?[- /.](\d)?/g, "");
        return string;
    }
};

module.exports = DateUtils;